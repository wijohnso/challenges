<h3>Challenges</h3>

These exercises are solutions in **C++** to [HackerRank Challenges](https://www.hackerrank.com/interview/interview-preparation-kit/ "Interview Prep Kit").  

Each solution has been written to satisfy the entire challenge as specified, though the driver programs have been modified to allow the programs to run on a standalone Linux system.  

Solutions can be accessed with the links below.

<h5>Warm-up Challenges</h5>

- [Sales By Match](https://gitlab.com/wijohnso/challenges/-/tree/master/Warm-up%20Challenges/Sales_By_Match) (Easy)  
- [Counting valleys](https://gitlab.com/wijohnso/challenges/-/tree/master/Warm-up%20Challenges/Counting%20Valleys) (Easy)  
- [Jumping on the Clouds](https://gitlab.com/wijohnso/challenges/-/tree/master/Warm-up%20Challenges/Jumping%20on%20the%20Clouds) (Easy)  
- [Repeated String](https://gitlab.com/wijohnso/challenges/-/tree/master/Warm-up%20Challenges/Repeated%20String) (Easy)  

<h5>Arrays</h5>

- [2D Array - DS](https://gitlab.com/wijohnso/challenges/-/tree/master/Arrays/2D%20Array%20-%20DS) (Easy)  
- [Arrays: left Rotation](https://gitlab.com/wijohnso/challenges/-/tree/master/Arrays/Left%20Rotation) (Easy)  
- [New Year Chaos](https://gitlab.com/wijohnso/challenges/-/tree/master/Arrays/New%20Year%20Chaos) (Medium)  
- [Minimum Swaps 2](https://gitlab.com/wijohnso/challenges/-/tree/master/Arrays/Minimum%20Swaps%202) (Medium)  
- [Array Manipulation](https://gitlab.com/wijohnso/challenges/-/tree/master/Arrays/Array%20Manipulation) (Hard)  

<h5>Dictionaries and Hashmaps</h5>

- [Hash Tables: Ransom Note](https://gitlab.com/wijohnso/challenges/-/tree/master/Dictionaries%20and%20Hashmaps/Hash%20Tables%3A%20Ransom%20Note) (Easy)  
- [Two Strings](https://gitlab.com/wijohnso/challenges/-/tree/master/Dictionaries%20and%20Hashmaps/Two%20Strings) (Easy)  
- [Sherlock and Anagrams](https://gitlab.com/wijohnso/challenges/-/tree/master/Dictionaries%20and%20Hashmaps/Sherlock%20and%20Anagrams) (Medium)  
- [Frequency Queries](https://gitlab.com/wijohnso/challenges/-/tree/master/Dictionaries%20and%20Hashmaps/Frequency%20Queries) (Medium)  
- [Count Triplets](https://gitlab.com/wijohnso/challenges/-/tree/master/Dictionaries%20and%20Hashmaps/Count%20Triplets) (Medium)  

<h5>Sorting</h5>

- [Sorting: Bubble Sort](https://gitlab.com/wijohnso/challenges/-/tree/master/Sorting/Sorting%3A%20Bubble%20Sort) (Easy)  
- [Mark and Toys](https://gitlab.com/wijohnso/challenges/-/tree/master/Sorting/Mark%20and%20Toys) (Easy)  
- [Sorting: Comparator](https://gitlab.com/wijohnso/challenges/-/tree/master/Sorting/Sorting%3A%20Comparator) (Medium)  
- [Fraudulent Activity Notifications](https://gitlab.com/wijohnso/challenges/-/tree/master/Sorting/Fraudulent%20Activity%20Notifications) (Medium)  
- [Merge Sort: Counting Inversions](https://gitlab.com/wijohnso/challenges/-/tree/master/Sorting/Merge%20Sort%3A%20Counting%20Inversions) (Hard)  

<h5>String Manipulation</h5>

- [Strings: Making Anagrams](https://gitlab.com/wijohnso/challenges/-/tree/master/String%20Manipulation/Strings%3A%20Making%20Anagrams) (Easy)  
- [Alternating Characters](https://gitlab.com/wijohnso/challenges/-/tree/master/String%20Manipulation/Alternating%20Characters) (Easy)  
- [Sherlock and the Valid String](https://gitlab.com/wijohnso/challenges/-/tree/master/String%20Manipulation/Sherlock%20and%20the%20Valid%20String) (Medium)  
- [Special String Again](https://gitlab.com/wijohnso/challenges/-/tree/master/String%20Manipulation/Special%20String%20Again) (Medium)  
- [Common Child](https://gitlab.com/wijohnso/challenges/-/tree/master/String%20Manipulation/Common%20Child) (Medium)  

<h5>Greedy Algorithms</h5>

- [Minimum Absolute Difference in an Array](https://gitlab.com/wijohnso/challenges/-/tree/master/Greedy%20Algorithms/Minimum_Difference) (Easy)  
- [Luck Balance](https://gitlab.com/wijohnso/challenges/-/tree/master/Greedy%20Algorithms/Luck%20Balance) (Easy)  
- [Greedy Florist](https://gitlab.com/wijohnso/challenges/-/tree/master/Greedy%20Algorithms/Greedy%20Florist) (Medium)  
- [Max Min](https://gitlab.com/wijohnso/challenges/-/tree/master/Greedy%20Algorithms/Max%20Min) (Medium)  
- [Reverse Shuffle Merge](https://gitlab.com/wijohnso/challenges/-/tree/master/Greedy%20Algorithms/Reverse%20Shuffle%20Merge) (Hard)  

<h5>Search</h5>

- [Hash Tables: Ice Cream Parlor](https://gitlab.com/wijohnso/challenges/-/tree/master/Search/Hash%20Tables%3A%20Ice%20Cream%20Parlor) (Medium)  
- Swap Nodes (Medium)  
- Pairs (Medium)  
- Triple Sum (Medium)  
- Minimum Time Required (Medium)  
- Maximum Subarray Sum (Hard)  
- Making Candies (Hard)  

<h5>Dynamic Programming</h5>

- Abbreviation (Medium)  
- Candies (Medium)  
- Decibinary Numbers (Hard)  

<h5>Stacks and Queues</h5>

- Queues: A Tale of Two Stacks (Medium)  
- Largest Rectangle (Medium)  
- Min Max Riddle (Medium)  
- Castle on the Grid (Medium)  
- Poisonous Plants (Hard)  

<h5>Graphs</h5>

- Find the Nearest Clone (Medium)  
- BFS: Shortest Reach in a Graph (Hard)  
- DNF: Connected Cell in a Grid (Hard)  
- Matrix (Hard)  

<h5>Trees</h5>

- Binary Search Tree: Lowest Common Ancestor (Easy)  
- Trees: Is this a Binary Search Tree? (Medium)  
- Tree: Huffman Decoding (Medium)  
- Balanced Forest (Hard)  

<h5>Linked Lists</h5>

- [Insert a Node at a Specific Position in a Linked List](https://gitlab.com/wijohnso/challenges/-/tree/master/Linked%20Lists/Insert%20a%20Node%20at%20a%20Specific%20Position%20in%20a%20Linked%20List) (Easy)  
- [Inserting a Node Into a Sorted Doubly Linked List](https://gitlab.com/wijohnso/challenges/-/tree/master/Linked%20Lists/Inserting%20a%20Node%20into%20a%20Sorted%20Doubly%20Linked%20List) (Easy)  
- [Reverse a Doubly Linked List](https://gitlab.com/wijohnso/challenges/-/tree/master/Linked%20Lists/Reverse%20a%20Doubly%20Linked%20List) (Easy)  
- [Find Merge Point of Two Lists](https://gitlab.com/wijohnso/challenges/-/tree/master/Linked%20Lists/Find%20Merge%20Point%20of%20Two%20Lists) (Easy)  
- [Linked Lists: Detect a Cycle](https://gitlab.com/wijohnso/challenges/-/tree/master/Linked%20Lists/Linked%20Lists%3A%20Detect%20a%20Cycle) (Easy)  

<h5>Recursion and Backtracking</h5>

- Recursion: Davis' Staircase (Medium)  
- Crossword Puzzle (Medium)  
- Recursive Digit Sum (Medium)  

<h5>Miscellaneous</h5>

- Time Complexity: Primality (Medium)  
- Friend Circle Queries (Medium)  
- Maximum XOR (Medium)  
