<h3>Frequency Queries</h3>

You are given **_q_** queries. Each query is of the form two integers described below:  
1. **_x_**: Insert one occurrence of x in your data structure.  
2. **_y_**: Delete one occurence of y from your data structure, if present.  
3. **_z_**: Check if any integer is present whose frequency is exactly **_z_**. If yes, print `1` else `0`.  

The queries are given in the form of a 2-D array **_queries_** of size **_q_** where **_queries[i][0]_** contains the operation, and **_queries[i][1]_** contains the data element. For example, you are given array **_queries = [(1,1),(2,2),(3,2),(1,1),(1,1),(2,1)(3,2)]_**. The results of each operation are:  

```
Operation   Array   Output
(1,1)       [1]
(2,2)       [1]
(3,2)                   0
(1,1)       [1,1]
(1,1)       [1,1,1]
(2,1)       [1,1]
(3,2)                   1
```

Return an array with the output: **_[0,1]_**.  

<h4>Function Description</h4>

Complete the `freqQuery` function. It must return an array of integers where each element is a **_1_** if there is at least one element value with the queried number of occurrences in the current array, or **_0_** if there is not.  

`freqQuery` has the following parameter(s):  

- `queries`: a 2-d array of integers  

<h4>Input Format</h4>

The first line contains of an integer **_q_**, the number of queries.  
Each of the next **_q_** lines contains two integers denoting the 2-d array **_queries_**.  

<h4>Constraints</h4>

- **_1 ≤ q ≤ 10<sup>5</sup>_**  
- **_1 ≤ x,y,z ≤ 10<sup>9</sup>_**  
- All **_queries[i][0] ∈ {1,2,3}_**  
- **_1 ≤ queries[i][1] ≤ 10<sup>9</sup>_**  

<h4>Output Format</h4>

Return an integer array consisting of all the outputs of queries of type **_3_**.  

<h4>Sample Input 0</h4>

```
8
1 5
1 6
3 2
1 10
1 10
1 6
2 5
3 2
```

<h4>Sample Output 0</h4>

```
0
1
```

<h4>Explanation 0</h4>

For the first query of type **_3_**, there is no integer whose frequency is **_2_**(**_array = [5,6]_**). So answer is **_0_**.  
For the second query of type **_3_**, there are two integers in **_array = [6,10,10,6]_** whose frequency is **_2_**(integers = **_6_** and **_10_**). So, the answer is **_1_**.  

<h4>Sample Input 1</h4>

```
4
3 4
2 1003
1 16
3 1
```

<h4>Sample Output 1</h4>

```
0
1
```

<h4>Explanation 1</h4>

For the first query of type **_3_**, there is no integer of frequency **-4_**. The answer is **_0_**. For the second query of type **_3_**, there is one integer, **_16_**, of frequency **_1_** so the answer is **_1_**.  

<h4>Sample Input 2</h4>

```
10
1 3
2 3
3 2
1 4
1 5
1 5
1 4
3 2
2 4
3 2
```

<h4>Sample Output 2

```
0
1
1
```

<h4>Explanation 2</h4>

When the first output query is run, the array is empty. We insert two **_4_**'s and two **_5_**'s before the second output query, **_arr = [4,5,5,4]_**, so there are two instances of elements occurring twice. We delete a **_4_** and run the same query. Now only the instances of **_5_** satisfy the query.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Frequency_Queries.cpp -o Frequency_Queries
```

This program can be run by calling:  

```
./Frequency_Queries
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Frequency_Queries
```
