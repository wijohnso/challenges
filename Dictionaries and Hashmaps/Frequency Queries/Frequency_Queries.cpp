#include <bits/stdc++.h>

#define INSERT 1
#define REMOVE 2

using namespace std;

vector<int> freqQuery(vector<vector<int>>);
string ltrim(const string &);
string rtrim(const string &);
vector<string> split(const string &);

int main() {
  string q_temp;
  getline(cin, q_temp);
  
  int q = stoi(ltrim(rtrim(q_temp)));
  
  vector<vector<int>> queries(q);
  
  for (int i = 0; i < q; i ++) {
    queries[i].resize(2);
    
    string queries_row_temp_temp;
    getline(cin, queries_row_temp_temp);
    
    vector<string> queries_row_temp = split(rtrim(queries_row_temp_temp));
    
    for (int j = 0; j < 2; j ++) {
      int queries_row_item = stoi(queries_row_temp[j]);
      queries[i][j] = queries_row_item;
    }
  }
  
  vector<int> ans = freqQuery(queries);
  
  for (int i = 0; i < ans.size(); i ++)
    cout << ans[i] << endl;
  
  return 0;
}

vector<int> freqQuery(vector<vector<int>> queries) {
  // local variables
  map<int, int>::iterator it;
  map<int, int>           buckets,        // counts indexed by occurrence
                          counts;         // counts indexed by value
  vector<int>             returnVec;      // vector to be returned
  int                     tempBucket,     // temporary bucket
                          tempCount;      // temporary count
  
  for (int i = 0; i < queries.size(); i ++) {
    int code = queries[i][0],
        value = queries[i][1];
    
    if (code == INSERT) {                                           // if inserting value
      if ((it = counts.find(value)) != counts.end()) {              // if value exists in counts
        tempCount = it->second;                                     // save tempCount and erase from counts
        counts.erase(it);
        
        it = buckets.find(tempCount);                               // find old count in buckets, then erase
        tempBucket = it->second;
        buckets.erase(it);
        if (--tempBucket > 0)                                       // reinsert decremented bucket if it exists
          buckets.insert(pair<int, int>(tempCount, tempBucket));
        
        counts.insert(pair<int, int>(value, ++tempCount));          // insert incremented count in counts
        
        if((it = buckets.find(tempCount)) != buckets.end()) {       // if buckets contains new count
          tempBucket = it->second;                                  // erase and reinsert incremented bucket
          buckets.erase(it);
          buckets.insert(pair<int, int>(tempCount, ++tempBucket));
        } else                                                      // if buckets does not contain new count
          buckets.insert(pair<int, int>(tempCount, 1));             // insert new count in buckets
        
      } else {                                                      // if value does not exist in counts
        counts.insert(pair<int, int>(value, 1));                    // insert the first instance of value in counts
        if ((it = buckets.find(1)) != buckets.end()) {              // if buckets already contains 1, replace with 2
          tempBucket = it->second;
          buckets.erase(it);
          buckets.insert(pair<int, int>(1, ++tempBucket));
        } else                                                      // if buckets does not contain new count, insert
          buckets.insert(pair<int, int>(1,1));
      }

    } else if (code == REMOVE) {                                    // if removing value
      if ((it = counts.find(value)) != counts.end()) {              // if value exists in counts
        tempCount = it->second;                                     // save and erase old count
        counts.erase(it);
        
        it = buckets.find(tempCount);                               // find old count in buckets, save and erase
        tempBucket = it->second;
        buckets.erase(it);
        if (--tempBucket > 0)                                       // if decremented bucket exists, insert 
          buckets.insert(pair<int, int>(tempCount, tempBucket));
        
        if (--tempCount > 0) {                                      // insert decremented count in counts
          counts.insert(pair<int, int>(value, tempCount));
          
          if ((it = buckets.find(tempCount)) != buckets.end()) {    // if buckets contains new count
            tempBucket = it->second;                                // erase and reinsert incremented bucket
            buckets.erase(it);
            buckets.insert(pair<int, int>(tempCount, ++tempBucket));
          } else                                                    // if buckets does not contain new count, insert
            buckets.insert(pair<int, int>(tempCount, 1));
        }
      }
      
    } else {                                                        // if counting value
      if ((it = buckets.find(value)) != buckets.end())
        returnVec.push_back(1);
      else
        returnVec.push_back(0);
      
    }
  }
  return returnVec;
}

string ltrim(const string &str) {
  string s(str);
  
  s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
  
  return s;
}

string rtrim(const string &str) {
  string s(str);
  
  s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),s.end());
  
  return s;
}

vector<string> split(const string &str) {
  vector<string> tokens;
  
  string::size_type start = 0;
  string::size_type end = 0;
  
  while ((end = str.find(" ", start)) != string::npos) {
    tokens.push_back(str.substr(start, end - start));
    start = end + 1;
  }
  
  tokens.push_back(str.substr(start));
  
  return tokens;
}
