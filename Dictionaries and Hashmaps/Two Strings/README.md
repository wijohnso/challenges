<h3>Two Strings</h3>

Given two strings, determine if they share a common substring. A substring may be as small as one character.  

<h4>Example</h4>

**_s1 = 'and'_**  
**_s2 = 'art'_**  
These share the common substring **_a_**.  

**_s1 = 'be_**  
**_s2 = 'cat'_**  
These do not share a substring.  

<h4>Function Description</h4>

Complete the function `twoStrings`.  

`twoStrings` has the following parameter(s):  

- `string s1`: a string  
- `string s2`: another string  

<h4>Returns</h4>

- `string`: either `YES` or `NO`  

<h4>Input Format</h4>

The first line contains a single integer **_p_**, the number of test cases.  

The following **_p_** pairs of lines are as follows:  

- The first line contains string **_s1_**.  
- The second line contains string **_s2_**.  

<h4>Constraints</h4>

- **_s1_** and **_s2_** consist of characters in the range ascii[a-z].  

<h4>Output Format</h4>

For each pair of strings, return `YES` or `NO`.  

<h4>Sample Input</h4>

```
2
hello
world
hi
world
```

<h4>Sample Output</h4>

```
YES
NO
```

<h4>Explanation</h4>

We have **_p = 2_** pairs to check:  
1. **_s1 = "hello"_**, **_s2 = "world"_**. The substrings **_"o"_** and **_"1"_** are common to both strings.  
2. **_a = "hi"_**, **_b = "world"_**. **_s1_** and **_s2_** share no common substrings.

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Two_Strings.cpp -o Two_Strings
```

This program can be run by calling:  

```
./Two_Strings
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Two_Strings
```
