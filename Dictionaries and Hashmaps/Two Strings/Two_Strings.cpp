#include <bits/stdc++.h>

using namespace std;

string twoStrings(string, string);

int main() {
  int q;
  cin >> q;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int q_itr = 0; q_itr < q; q_itr ++) {
    string s1;
    getline(cin, s1);
    
    string s2;
    getline(cin, s2);
    
    string result = twoStrings(s1, s2);
    
    cout << result << endl;
  }
  
  return 0;
}

string twoStrings(string s1, string s2) {
  for (auto c : s1) {                   // for each substring in s1
    if (s2.find(c) != string::npos) {   // search s2
      return "YES";
    }
  }
  return "NO";
}
