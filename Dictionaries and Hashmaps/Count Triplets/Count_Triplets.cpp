#include <bits/stdc++.h>

using namespace std;

long countTriplets(vector<long>, long);
long countTriplets2(vector<long>, long);
string ltrim(const string &);
string rtrim(const string &);
vector<string> split(const string &);

int main() {
  string nr_temp;
  getline(cin, nr_temp);
  vector<string> nr = split(rtrim(nr_temp));
  int n = stol(nr[0]),
      r = stol(nr[1]);
      
  string arr_temp_temp;
  getline(cin, arr_temp_temp);
  vector<string> arr_temp = split(rtrim(arr_temp_temp));
  
  vector<long> arr(n);
  for (int i = 0; i < n; i ++) {
    long arr_item = stol(arr_temp[i]);
    arr[i] = arr_item;
  }
  
  long ans = countTriplets(arr, r);
  cout << ans << endl;
  
  return 0;
}

long countTriplets(vector<long> arr, long r) {
  map<long, int> right,
                  left;
  map<long,int>::iterator leftIt,
                          rightIt;
  long size = arr.size(),
       numVals,
       searchVal,
       returnVal = 0;
  
  // populating left map
  for (int i = 0; i < size; i ++) {
    if ((leftIt = left.find(arr[i])) == left.end()) {
      left.insert(pair<long, int>(arr[i], 1));
    } else {
      numVals = leftIt->second;
      left.erase(arr[i]);
      left.insert(pair<long, int>(arr[i], ++numVals));
    }
  }
  
  for (int i = size - 1; i >= 0; i --) {
    // remove or decrement current value from left map
    leftIt = left.find(arr[i]);
    numVals = leftIt->second;
    left.erase(arr[i]);
    if (--numVals > 0) {
      left.insert(pair<long, int>(arr[i], numVals));
    }
    
    // search for triplet and update returnVal
    if (arr[i] % r == 0) {
      searchVal = arr[i] / r;
      if ((leftIt = left.find(searchVal)) != left.end()) {
        if ((rightIt = right.find(arr[i] * r)) != right.end()) {
          returnVal += (rightIt->second * leftIt->second);
        }
      }
    }
    
    // move current value to right map
    if ((rightIt = right.find(arr[i])) == right.end()) {
      right.insert(pair<long, int>(arr[i], 1));
    } else {
      numVals = rightIt->second;
      right.erase(arr[i]);
      right.insert(pair<long, int>(arr[i], ++numVals));
    }
  }
  
  return returnVal;
}

string ltrim(const string &str) {
  string s(str);
  
  s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
  
  return s;
}

string rtrim(const string &str) {
  string s(str);
  
  s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),s.end());
  
  return s;
}

vector<string> split(const string &str) {
  vector<string> tokens;
  
  string::size_type start = 0;
  string::size_type end = 0;
  
  while ((end = str.find(" ", start)) != string::npos) {
    tokens.push_back(str.substr(start, end - start));
    start = end + 1;
  }
  
  tokens.push_back(str.substr(start));
  
  return tokens;
}
