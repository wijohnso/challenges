<h3>Count Triplets</h3>

You are given an array and you need to find number of tripets of indices **_(i,j,k)_** such that the elements at those indices are in geometric progression for a given common ratio **_r_** and **_i < j < k_**.  

For example, **_arr = [1,4,16,64]_**. If **_r = 4_**, we have **_[1,4,16]_** and **_[4,16,64]_** at indices **_(0,1,2)_** and **_(1,2,3)_**.  

<h4>Function Description</h4>

Complete the `countTriplets` function. It should return the number of triplets forming a geometric progression for a given **_r_** as an integer.  

`countTriplets` has the following parameter(s):  

- `arr`: an array of integers  
- `r`: an integer, the common ratio  

<h4>Input Format</h4>

The first line contains two space-separated integers **_n_** and **_r_**, the size of **_arr_** and the common ratio.  
The next line contains **_n_** space-seperated integers **_arr[i]_**.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 10<sup>5</sup>_**  
- **_1 ≤ r ≤ 10<sup>9</sup>_**  
- **_1 ≤ arr[i] ≤ 10<sup>9</sup>_**  

<h4>Output Format</h4>

Return the count of triplets that form a geometric progression.  

<h4>Sample Input 0</h4>

```
4 2
1 2 2 4
```

<h4>Sample Output 0</h4>

```
2
```

<h4>Explanation 0</h4>

There are **_2_** triplets in satisfying our criteria, whose indices are **_(0,1,3)_** and **_(0,2,3)_**.  

<h4>Sample Input 1</h4>

```
6 3
1 3 9 9 27 81
```

<h4>Sample Output 1</h4>

```
6
```

<h4>Explanation 1</h4>

The triplets satisfying are index **_(0,1,2)_**, **_(0,1,3)_**, **_(1,2,4)_**, **_(1,3,4)_**, **_(2,4,5)_**, and **_(3,4,5)_**.  

<h4>Sample Input 2</h4>

```
5 5
1 5 5 25 125
```

<h4>Sample Output 2</h4>

```
4
```

<h4>Explanation 2</h4>

The triplets satisfying are index **_(0,1,3)_**, **_(0,2,3)_**, **_(1,3,4)_** and **_(2,3,4)_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Count_Triplets.cpp -o Count_Triplets
```

This program can be run by calling:  

```
./Count_Triplets
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Count_Triplets
```
