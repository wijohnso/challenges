<h3>Sherlock and Anagrams</h3>

Two strings are anagrams of each other if the letters of one string can be rearranged to form the other string. Given a string, find the number of pairs of substrings of the string that are anagrams of each other.  

<h4>Example</h4>

**_s = mom_**  
The list of all anagrammatic pairs is **_[m,m][mo,om]_** at positions **_[[0],[2]],[[0,1],[1,2]]_** respectively.  

<h4>Function Description</h4>

Complete the function `sherlockAndAnagrams`.  
`sherlockAndAnagrams` has the following parameter(s):  

- `string s`: a string  

<h4>Returns</h4>

- `int`: the number of unordered anagrammatic pairs of substrings in **_s_**  

<h4>Input Format</h4>

The first line contains an integer **_q_**, the number of queries.  
Each of the next **_q_** lines contains a string **_s_** to analyze.  

<h4>Constraints</h4>

- **_1 ≤ q ≤ 10_**  
- **_2 ≤ length of s ≤ 100_**  
- **_s_** contains only lowercase letters in the range ascii[a-z].  

<h4>Sample Input 0</h4>

```
2
abba
abcd
```

<h4>Sample Output 0</h4>

```
4
0
```

<h4>Explanation 0</h4>

The list of all anagrammatic pairs is**_[a,a],[ab,ba],[b,b]_** and **_[abb,bba]_** at positions **_[[0],[3]],[[0,1],[2,3]],[[1],[2]]_** and **_[[0,1,2],[1,2,3]]_** respectively.  

No anagrammatic pairs exist in the second query as no character repeats.  

<h4>Sample Input 1</h4>

```
2
ifailuhkqq
kkkk
```

<h4>Sample Output 1</h4>

```
3
10
```

<h4>Explanation 1</h4>

For the first query, we have anagram pairs **_[i,i],[a,a]_** and **_[ifa,fai]_** at positions **_[[0],[3]],[[8],[9]]_** and **_[[0,1,2],[1,2,3]]_** respectively.  

For the second query:  
There are 6 anagrams of the form **_[k,k]_** at positions **_[[0],[1]],[[0],[2]],[[0],[3]],[[1],[3]],[[2],[3]]_** and **_[[2],[3]]_**.  
There are 3 anagrams of the form **_[kk,kk]_** at positions **_[[0,1],[1,2]],[[0,1],[2,3]]_** and **_[[1,2],[2,3]]_**.  
There is 1 anagram of the form **_[kkk,kkk]_** at position **_[[0,1,2],[1,2,3]]_**.  

<h4>Sample Input 2</h4>

```
1
cdcd
```

<h4>Sample Output 2

```
5
```

<h4>Explanation 2</h4>

There are two anagrammatic pairs of length **_1_**: **_[c,c]_** and **_[d,d]_**.  
There are three anagrammatic pairs of length **_2_**: **_[cd,dc],[cd,cd],[dc,dc]_** at positions **_[[0,1],[1,2]],[[0,1],[2,3]],[[1,2],[2,3]]_** respectively.   

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Sherlock_and_Anagrams.cpp -o Sherlock_and_Anagrams
```

This program can be run by calling:  

```
./Sherlock_and_Anagrams
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Sherlock_and_Anagrams
```
