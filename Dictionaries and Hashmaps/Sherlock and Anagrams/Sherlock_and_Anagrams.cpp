#include <bits/stdc++.h>

using namespace std;

int sherlockAndAnagrams(string);

int main() {
  int q;
  cin >> q;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int q_itr = 0; q_itr < q; q_itr ++) {
    string s;
    getline(cin, s);
    
    int result = sherlockAndAnagrams(s);
    
    cout << result << endl;
  }
  
  return 0;
}

int sherlockAndAnagrams(string s) {
  // local variables
  vector<string> vec;
  int count = 0;
  
  for (int i = 0; i < s.size(); i ++) {         // for each character in s
    for (int j = i + 1; j <= s.size(); j ++) {  // for each unchecked character in s
      // checking single-character anagrams
      if (j < s.size() &&
          s[i] == s[j])                         // if characters match
        count ++;                               // increment count
      
      // adding substrings to vector
      if (j - i > 1) {                          // if width > 2
        string s1 = s.substr(i, j - i);         // create substring
        sort(s1.begin(), s1.end());             // sort substring alphabetically
        vec.push_back(s1);                      // add substring to vector
      }
    }
  }
  
  sort(vec.begin(), vec.end());                 // sort substrings
  
  // checking multiple-character anagrams
  while (vec.size() > 1) {                      // for each substring in vector
    string s1 = vec[vec.size() - 1];            // pop substring from end of vector
    vec.pop_back();
    int tempCount = 0;
    
    while(!vec.empty() &&                       // while vector is not empty
          s1 == vec[vec.size() - 1]) {          // while current string equals s1
      tempCount ++;                             // increment tempCount
      vec.pop_back();                           // pop current string
    }
    
    for (int i = 1; i <= tempCount; i ++)       // for each tempCount
      count += i;                               // add duplicate pairs
  }
  
  return count;
}
