<h3>Hash Tables: Ransom Note</h3>

Harold is a kidnapper who wrote a ransom note, but now he is worried it will be traced back to him through his handwriting. He found a magazine and wants to know if he can cut out whole words from it and use them to create an untraceable replica of his ransom note. The words in his note are case-sensitive and he must use only whole words available in the magazine. He cannot use substrings or concatenation to create the words he needs.  

Given the words in the magazine and the words in the ransom note, print `Yes` if he can replicate his ransom note exactly using whole words from the magazine; otherwise, print `No`.  

For example, the note is "Attack at dawn". The magazine contains only "attack at dawn". The magazine has all the right words, but there's a case mismatch. The answer is **_No_**.  

<h4>Function Description</h4>

Complete the `checkMagazine` function. It must print **_Yes_** if the note can be formed using the magazine, or **_No_**.  

`checkMagazine` has the following parameters:  

- `magazine`: an array of strings, each a word in the magazine  
- `note`: an array of strings, each a word in the ransom note  

<h4>Input Format</h4>

The first line contains two space-separated integers, **_m_** and **_n_**, the numbers of words in the **_magazine_** and the **_note_**.  
The second line contains **_m_** space-separated strings, each **_magazine[i]_**.  
The third line contains **_n_** space-separated strings, each **_note[i]_**.  

<h4>Constraints</h4>

- **_1 ≤ m,n ≤ 30000_**  
- **_1 ≤ |magazine[i]|,|note[i]| ≤ 5_**  
- Each word consists of English alphabetic letters (i.e., **_a_** to **_z_** and **_A_** to **_Z_**).  

<h4>Output Format</h4>

Print `Yes` if he can use the magazine to create an untraceable replica of his ransom note. Otherwise, print `No`.  

<h4>Sample Input 0</h4>

```
6 4
give me one grand today night
give one grand today
```

<h4>Sample Output 0</h4>

```
Yes
```

<h4>Sample Input 1</h4>

```
6 5
two times three is not four
two times two is four
```

<h4>Sample Output 1</h4>

```
No
```

<h4>Explanation 1</h4>

'two' only occurs once in the magazine.  

<h4>Sample Input 2</h4>

```
7 4
ive got a lovely bunch of coconuts
ive got some coconuts
```

<h4>Sample Output 2

```
No
```

<h4>Explanation 2</h4>

Harold's magazine is missing the word **_some_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Hash_Tables_Ransome_Note.cpp -o Hash_Tables_Ransome_Note
```

This program can be run by calling:  

```
./Hash_Tables_Ransome_Note
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Hash_Tables_Ransome_Note
```
