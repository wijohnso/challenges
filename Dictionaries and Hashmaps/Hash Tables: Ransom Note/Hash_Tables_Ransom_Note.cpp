#include <bits/stdc++.h>

using namespace std;

struct entry {
  string Word;
  entry *next;
  int count;
};

void checkMagazine(vector<string>, vector<string>);
vector<string> split_string(string);

int main() {
  string mn_temp;
  getline(cin, mn_temp);
  
  vector<string> mn = split_string(mn_temp);
  
  int m = stoi(mn[0]),
      n = stoi(mn[1]);
  
  string magazine_temp_temp;
  getline(cin, magazine_temp_temp);
  vector<string> magazine_temp = split_string(magazine_temp_temp);
  vector<string> magazine(m);
  for (int i = 0; i < m; i ++) {
    string magazine_item = magazine_temp[i];
    magazine[i] = magazine_item;
  }
  
  string note_temp_temp;
  getline(cin, note_temp_temp);
  vector<string> note_temp = split_string(note_temp_temp);
  vector<string> note(n);
  for (int i = 0; i < n; i ++) {
    string note_item = note_temp[i];
    note[i] = note_item;
  }
  
  checkMagazine(magazine, note);
}

void checkMagazine(vector<string> magazine, vector<string> note) {
  // local variables
  map<string, int> dictionary;
  bool found = true;
  
  for (int i = 0; i < magazine.size(); i ++)  // for each word in magazine
    dictionary[magazine[i]] ++;               // add to dictionary
  
  for (int i = 0; i < note.size(); i ++) {    // for each word in note
    auto it = dictionary.find(note[i]);       // try to find word in dictionary
    if (it == dictionary.end() ||             // if word not found
        (--it->second) < 0) {                 // if not enough instances in dictionary
      found = false;
      break;
    }
  }
  if (found)
    cout << "Yes";
  else
    cout << "No";
  
  return;
}

vector<string> split_string(string input_string) {
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {return x == y and x ==' ';});
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == ' ') 
    input_string.pop_back();
  
  vector<string> splits;
  char delimiter = ' ';
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
