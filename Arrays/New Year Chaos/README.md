<h3>New Year Chaos</h3>

It is New Year's Day and people are in line for the Wonderland rollercoaster ride. Each person wears a sticker indicating their initial position in the queue. Initial positions increment by from **_1_** at the front of the line to **_n_** at the back.  

Any person in the queue can bribe the person directly in front of them to swap positions. If two people swap positions, they still wear the same sticker denoting their original places in line. One person can bribe at most two others. For example, if **_n = 8_** and **_Person 5_** bribes **_Person 4_**, the queue will look like this: **_1,2,3,5,4,6,7,8_**.  

Fascinated by this chaotic queue, you decide you must know the minimum number of bribes that took place to get the queue into its current state. If anyone has bribed more than two people, the line is too chaotic to compute the answer.  

<h4>Function Description</h4>

Complete the function `minimumBribes`.  

`minimumBribes` has the following parameter(s):  

- `int q[n]`: the positions of the people after all bribes  

<h4>Returns</h4>

- No value is returned. Print the minimum number of bribes necessary or `Too chaotic` if someone has bribed more than **_2_** people.  

<h4>Input Format</h4>

The first line contains an integer **_t_**, the number of test cases.  
Each of the next **_t_** pairs of lines are as follows:  
- The first line contains an integer **_t_**, the number of people in the queue.  
- The second line has **_n_** space-separated integers describing the final state of the queue.  

<h4>Constraints</h4>

- **_1 ≤ t ≤ 10_**  
- **_1 ≤ n ≤ 10<sup>5</sup>_**  

<h4>Subtasks</h4>

For **_60%_** score **_1 ≤ n ≤ 10<sup>5</sup>_**  
For **_100%_** score **_1 ≤ n ≤ 10<sup>5</sup>_**  

<h4>Sample Input</h4>

```
2
5
2 1 5 3 4
5
2 5 1 3 4
```

<h4>Sample Output</h4>

```
3
Too chaotic
```

<h4>Explanation</h4>

**_Test Case 1_**  

The initial state:  
After person **_5_** moves one position ahead by bribing person **_4_**:  

```
1 2 3 5 4
```

Now person **_5_** moves another position ahead by bribing person **_3_**:  

```
1 2 5 3 4
```

And person **_2_** moves one position ahead by bribing person **_1_** so the final state is:  

```
2 1 5 3 4
```

after three bribing operations.  

**_Test Case 2_**  

No person can bribe more than two people, yet it appears person **_5_** has done so. It is not possible to achieve the input state.

```
Too chaotic
```

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ New_Year_Chaos.cpp -o New_Year_Chaos
```

This program can be run by calling:  

```
./New_Year_Chaos
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./New_Year_Chaos
