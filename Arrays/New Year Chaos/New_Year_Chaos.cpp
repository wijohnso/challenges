#include <bits/stdc++.h>

using namespace std;

void minimumBribes(vector<int>);
vector<string> split_string(string);

int main() {
  int t;
  cin >> t;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int t_itr = 0; t_itr < t; t_itr ++) {
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    string q_temp_temp;
    getline(cin, q_temp_temp);
    
    vector<string> q_temp = split_string(q_temp_temp);
    
    vector<int> q(n);
    
    for (int i = 0; i < n; i ++) {
      int q_item = stoi(q_temp[i]);
      q[i] = q_item;
    }
    
    minimumBribes(q);
  }
  
  return 0;
}

void minimumBribes(vector<int> A) {
  // local variable
  int bribes = 0;
  
  for (int j = 0; j < 2; j ++) {                // for each possible bribe
    for (int i = A.size() - 1; i > 0; i --) {   // for each person in line
      if (A[i] < A[i-1]) {                      // if the array is out of order
        bribes ++;                              // swap elements
        int temp = A[i];
        A[i] = A[i - 1];
        A[i - 1] = temp;
      }
    }
  }
  
  for(int i = 0; i < A.size() - 1; i ++) {      // for each person in line
    if (A[i] > A[i + 1]) {                      // if the line is out of order
      cout << "Too chaotic" << endl;            // at least one person must have
      break;                                    // swapped more than two times
    } else if (i == A.size() - 2) {             // if this is the end of the loop
      cout << bribes << endl;                   // print the result
    }
  }
  return;
}

vector<string> split_string(string input_string) {
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {return x == y and x ==' ';});
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == ' ') 
    input_string.pop_back();
  
  vector<string> splits;
  char delimiter = ' ';
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
