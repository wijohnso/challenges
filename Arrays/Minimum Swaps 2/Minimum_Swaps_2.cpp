#include <bits/stdc++.h>

using namespace std;

int minimumSwaps(vector<int>);
vector<string> split_string(string);

int main () {
  int n;
  cin >> n;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  string arr_temp_temp;
  getline(cin, arr_temp_temp);
  
  vector<string> arr_temp = split_string(arr_temp_temp);
  
  vector<int> arr(n);
  
  for (int i = 0; i < n; i ++) {
    int arr_item = stoi(arr_temp[i]);
    arr[i] = arr_item;
  }
  
  int res = minimumSwaps(arr);
  
  cout << res << endl;
  
  return 0;
}

int minimumSwaps(vector<int> arr) {
  // local variable
  int swaps = 0;
  
  for (int j = 0; j < arr.size()-1; j ++) {   // for all array elements except the last
    for (int i = j; i < arr.size(); i ++) {   // for all unsorted array elements
      if (arr[j] == j + 1)                    // if element is already sorted
        break;                                // don't count
      if (arr[i] == j + 1) {                  // if this is the next element to swap
        int temp = arr[j];                    // swap
        arr[j] = arr[i];
        arr[i] = temp;
        swaps ++;
      }
    }
  }
  return swaps;
}

vector<string> split_string(string input_string) {
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {return x == y and x ==' ';});
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == ' ') 
    input_string.pop_back();
  
  vector<string> splits;
  char delimiter = ' ';
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
