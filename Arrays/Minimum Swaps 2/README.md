<h3>Minimum Swaps 2</h3>

You are given an unordered array consisting of consecutive integers ∈ [1, 2, 3, ..., n] without any duplicates. You are allowed to swap any two elements. You need to find the minimum number of swaps required to sort the array in ascending order.  

For example, given the array **_arr = [7,1,3,2,4,5,6]_** we perform the following steps:

```
i   arr                     swap (indices)
0   [7, 1, 3, 2, 4, 5, 6]   swap (0,3)
1   [2, 1, 3, 7, 4, 5, 6]   swap (0,1)
2   [1, 2, 3, 7, 4, 5, 6]   swap (3,4)
3   [1, 2, 3, 4, 7, 5, 6]   swap (4,5)
4   [1, 2, 3, 4, 5, 7, 6]   swap (5,6)
5   [1, 2, 3, 4, 5, 6, 7]
```

It took **_5_** swaps to sort the array.  

<h4>Function Description</h4>

Complete the function `minimumSwaps`. It must return an integer representing the minimum number of swaps to sort the array.  

`minimumSwaps` has the following parameter(s):  

- `arr`: an unordered array of integers  

<h4>Input Format</h4>

The first line contains an integer, **_n_**, the size of **_arr_**.  
The second line contains **_n_** space-separated integers **_arr[i]_**.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 10<sup>5</sup>_**  
- **_1 ≤ arr[i] ≤ n_**  

<h4>Output Format</h4>

Return the minimum number of swaps to sort the given array.  

<h4>Sample Input 0</h4>

```
4
4 3 1 2
```

<h4>Sample Output 0</h4>

```
3
```

<h4>Explanation 0</h4>

Given array **_arr: [4,3,1,2]_**  
After swapping **_(0,2)_** we get **_arr: [1,3,4,2]_**  
After swapping **_(1,2)_** we get **_arr: [1,4,3,2]_**  
After swapping **_(1,3)_** we get **_arr: [1,2,3,4]_**  
So, we need a minimum of **_3_** swaps to sort the array in ascending order.  

<h4>Sample Input 1</h4>

```
5
2 3 4 1 5
```

<h4>Sample Output 1</h4>

```
3
```

<h4>Explanation 1</h4>

Given array **_arr: [2,3,4,1,5]_**  
After swapping **_(2,3)_** we get **_arr: [2,3,4,1,5]_**  
After swapping **_(0,1)_** we get **_arr: [3,2,1,4,5]_**  
After swapping **_(0,2)_** we get **_arr: [1,2,3,4,5]_**  
So, we need a minimum of **_3_** swaps to sort the array in ascending order.  

<h4>Sample Input 2</h4>

```
7
1 3 5 2 4 6 7
```

<h4>Sample Output 2</h4>

```
3
```

<h4>Explanation 2</h4>

Given array **_arr: [1,3,5,2,4,6,7]_**  
After swapping **_(1,3)_** we get **_arr: [1,2,5,3,4,6,7]_**  
After swapping **_(2,3)_** we get **_arr: [1,2,3,5,4,6,7]_**  
After swapping **_(3,4)_** we get **_arr: [1,2,3,4,5,6,7]_**  
So, we need a minimum of **_3_** swaps to sort the array in ascending order.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Minimum_Swaps_2.cpp -o Minimum_Swaps_2
```

This program can be run by calling:  

```
./Minimum_Swaps_2
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Minimum_Swaps_2
