#include <bits/stdc++.h>

#define SIZE 6    // width and height of 2d array input

using namespace std;

int hourglassSum(vector<vector<int>>);

int main() {
  vector<vector<int>> arr(SIZE);
  
  for (int i = 0; i < SIZE; i ++) {
    arr[i].resize(6);
    
    for (int j = 0; j < SIZE; j ++) {
      cin >> arr[i][j];
    }
    
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
  }
  
  int result = hourglassSum(arr);
  
  cout << result << endl;
  
  return 0;
}

int hourglassSum(vector<vector<int>> arr) {
  // local variables
  int current = 0,
      max = 0;
  
  for (int i = 0; i < SIZE - 2; i ++) {                       // for each column
    for (int j = 0; j < SIZE - 2; j ++) {                     // for each row
      current = arr[i][j] +   arr[i][j+1] +   arr[i][j+2] +   // calculate hourglass sum
                              arr[i+1][j+1] +
                arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2];
      if ((i == 0 && j == 0) || current > max)                // update maximum
        max = current;
    }
  }
  return max;
}
