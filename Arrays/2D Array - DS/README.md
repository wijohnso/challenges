<h3>2D Array - DS</h3>

Given a **_6 x 6_** 2D Array, **_arr_**:  

```
1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
```

An hourglass in **_arr_** is a subset of values with indices falling in this pattern in **_arr_**'s graphical representation:  

```
a b c
  d
e f g
```

There are **_16_** hourglasses in **_arr_**. An hourglass sum is the sum of an hourglass' values. Calculate the hourglass sum for every hourglass in **_arr_**, then print the maximum hourglass sum. The array will always be **_6 x 6_**.  

<h4>Example</h4>

```
-9 -9 -9  1 1 1 
 0 -9  0  4 3 2
-9 -9 -9  1 2 3
 0  0  8  6 6 0
 0  0  0 -2 0 0
 0  0  1  2 4 0
```
 
The **_16_** hourglass sums are:  

```
-63, -34, -9, 12, 
-10,   0, 28, 23, 
-27, -11, -2, 10, 
  9,  17, 25, 18
```

The highest hourglass sum is **_28_** from the hourglass beginning at row **_1_**, column **_2_**:  

```
0 4 3
  1
8 6 6
```

<h4>Function Description</h4>

Complete the function `hourglassSum`.  

`hourglassSum` has the following parameter(s):  

- `int arr[6][6]`: an array of integers  

<h4>Returns</h4>

- `int`: the maximum hourglass sum  

<h4>Input Format</h4>

Each of the **_6_** lines of inputs **_arr[i]_** contains space-separated integers **_arr[i][j]_**.  

<h4>Constraints</h4>

- **_-9 ≤ arr[i][j] ≤ 9_**  
- **_0 ≤ i_**, **_j ≤ 5_**  

<h4>Output Format</h4>

Print the largest (maximum) hourglass sum found in **_arr_**.  

<h4>Sample Input</h4>

```
1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 2 4 4 0
0 0 0 2 0 0
0 0 1 2 4 0
```

<h4>Sample Output</h4>

```
19
```

<h4>Explanation</h4>

**_arr_** contains the following hourglasses:  

```
1 1 1
  1
1 1 1

1 1 0
  0
1 1 0

1 0 0
  0
1 0 0

0 0 0
  0
0 0 0


0 1 0
  1
0 0 2

1 0 0
  1
0 2 4

0 0 0
  0
2 4 4

0 0 0
  0
4 4 0


1 1 1
  0
0 0 0

1 1 0
  2
0 0 2

1 0 0
  4
0 2 0

0 0 0
  4
2 0 0


0 0 2
  0
0 0 1

0 2 4
  0
0 1 2
  
2 4 4
  2
1 2 4
  
4 4 0
  0
2 4 0
```

The hourglass with the maximum sum (**_19_**) is:  

```
2 4 4
  2
1 2 4
```

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ 2d_Array_DS.cpp -o 2d_Array_DS
```

This program can be run by calling:  

```
./2d_Array_DS
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./2d_Array_DS
```
