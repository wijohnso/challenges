#include <bits/stdc++.h>

using namespace std;

long arrayManipulation(int, vector<vector<int>>);
vector<string> split_string(string);

int main() {
  string nm_temp;
  getline(cin, nm_temp);
  
  vector<string> nm = split_string(nm_temp);
  
  int n = stoi(nm[0]),
      m = stoi(nm[1]);
  
  vector<vector<int>> queries(m);
  for (int i = 0; i < m; i ++) {
    queries[i].resize(3);
    
    for (int j = 0; j < 3; j ++)
      cin >> queries[i][j];
    
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
  }
  
  long result = arrayManipulation(n, queries);
  
  cout << result << endl;
  
  return 0;
}

/*!
 * @brief This solution tracks changes in array values reading from left to right.
 * 
 * @return the maximum sum across the array when adding from left to right
 * 
 * @example 
 *    input: 5 3
 *           1 2 100
 *           2 5 100
 *           3 4 100
 *    iter  0    1    2    3    4
 *    ----------------------------------
 *    0     0    0    0    0    0
 *    1     100  0   -100  0    0
 *    2     100  100 -100  0    0
 *    3     100  100 0     0    -100
 * 
 *    max   0    1    2    3    4
 *    ----------------------------------
 *          100  200  200  200  100
 *    Therefore the return value is 200
 */
long arrayManipulation(int n, vector<vector<int>> queries) {
  // local variables
  long *arr = (long *)calloc(n, sizeof(long)),
       front,
       back,
       value,
       max = 0,
       current = 0;
       
  for (int i = 0; i < queries.size(); i ++) {
    front = queries[i][0];        // gather input parameters
    back = queries[i][1];
    value = queries[i][2];
    
    arr[front - 1] += value;      // populate tracking values
    if (back < n)
      arr[back] -= value;
  }
  
  for (int i = 0; i < n; i ++) {  // calculate maximum
    current += arr[i];
    if (current > max)
      max = current;
  }
  
  free(arr);
  
  return max;
}

vector<string> split_string(string input_string) {
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {return x == y and x ==' ';});
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == ' ') 
    input_string.pop_back();
  
  vector<string> splits;
  char delimiter = ' ';
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
