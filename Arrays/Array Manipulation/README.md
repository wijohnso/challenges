<h3>Array Manipulation</h3>

Starting with a 1-indexed array of zeros and a list of operations, for each operation add a value to each of the array element between two given indices, inclusive. Once all operations have been performed, return the maximum value in the array.  

<h4>Example</h4>

**_n = 10_**  
**_queries = [[1,5,3],[4,8,7],[6,9,1]]_**  

Queries are interpreted as follows:  

```
a b k
1 5 3
4 8 7
6 9 1
```

Add the values of **_k_** between the indices **_a_** and **_b_** inclusive:  

```
index->	 1  2  3  4  5  6  7  8  9 10
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        [3, 3 ,3, 3, 3, 0, 0, 0, 0, 0]
        [3, 3, 3,10,10, 7, 7, 7, 0, 0]
        [3, 3, 3,10,10, 8, 8, 8, 1, 0]
```

The largest value is **_10_** after all operations are performed.  

<h4>Function Description</h4>

Complete the function `arrayManipulation`.  

`arrayManipulation` has the following parameters:  

- `int n`: the number of elements in the array  
- `int queries[q][3]`: a two dimensional array of queries where each queries[i] contains three integers, a, b, and k.  

<h4>Returns</h4>

- `int`: the maximum value in the resultant array  

<h4>Input Format</h4>

The first line contains two space-separated integers **_n_** and **_m_**, the size of the array and the number of operations.  
Each of the next lines contains three space-separated integers **_a_**, **_b_** and **_k_**, the left index, right index and summand.  

<h4>Constraints</h4>

- **_3 ≤ n ≤ 10<sup>7</sup>_**  
- **_1 ≤ m ≤ 2·10<sup>5</sup>_**  
- **_1 ≤ a ≤ b ≤ n_**  
- **_0 ≤ k ≤ 10<sup>9</sup>_**

<h4>Sample Input</h4>

```
5 3
1 2 100
2 5 100
3 4 100
```

<h4>Sample Output</h4>

```
200
```

<h4>Explanation</h4>

After the first update the array is **_[100,100,0,0,0]_**.  
After the second update list is **_[100,200,100,100,100]_**  
After the third update list is **_[100,200,200,200,100]_**  

The maximum value is **_200_**.

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Array_Manipulation.cpp -o Array_Manipulation
```

This program can be run by calling:  

```
./Array_Manipulation
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Array_Manipulation
```
