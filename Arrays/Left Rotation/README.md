<h3>Arrays: Left Rotation</h3>

A left rotation operation on an array shifts each of the array's elements **_1_** unit to the left. For example, if **_2_** left rotations are performed on array **_[1.2.3.4.5]_**, then the array would become **_[3,4,5,1,2]_**. Note that the lowest index item moves to the highest index in a rotation. This is called a circular array.  

Given an array **_a_** of **_n_** integers and a number, **_d_**, perform **_d_** left rotations on the array. Return the updated array to be printed as a single line of space-separated integers.  

<h4>Function Description</h4>

Complete the function `rotLeft`.  

`rotLeft` has the following parameter(s):  

- `int a[n]`: the array to rotate  
- `int d`: the number of rotations  

<h4>Returns</h4>

- `int a'[n]`: the rotated array  

<h4>Input Format</h4>

The first line contains two space-separated integers **_n_** and **_d_**, the size of **_a_** and the number of left rotations.  
The second line contains **_n_** space-separated integers, each an **_a[i]_**.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 10<sup>5</sup>_**  
- **_1 ≤ d ≤ n_**  
- **_1 ≤ a[i] ≤ 10<sup>6</sup>_**  

<h4>Sample Input</h4>

```
5 4
1 2 3 4 5
```

<h4>Sample Output</h4>

```
5 1 2 3 4
```

<h4>Explanation</h4>

When we perform**_d = 4_** left rotations, the array undergoes the following sequence of changes:  
**_[1,2,3,4,5] → [2,3,4,5,1] → [3,4,5,1,2] → [4,5,1,2,3] → [5,1,2,3,4]_**  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Left_Rotation.cpp -o Left_Rotation
```

This program can be run by calling:  

```
./Left_Rotation
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Left_Rotation
```
