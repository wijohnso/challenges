<h3>Hash Tables: Ice Cream Parlor</h3>

Each time Sunny and Johnny take a trip to the Ice Cream Parlor, they pool their money to buy ice cream. On any given day, the parlor offers a line of flavors. Each flavor has a cost associated with it.  

Given the value of **_money_** and the **_cost_** of each flavor for **_t_** trips to the Ice Cream Parlor, help Sunny and Johnny choose two distinct flavors such that they spend their entire pool of money during each visit. ID numbers are the 1- based index number associated with a **_cost_**. For each trip to the parlor, print the ID numbers for the two types of ice cream that Sunny and Johnny purchase as two space-separated integers on a new line. You must print the smaller ID first and the larger ID second.  

For example, there are **_n = 5_** flavors having **_cost = [2,1,3,5,6]_**. Together they have **_money = 5_** to spend. They would purchase flavor ID's **_1_** and **_3_** for a cost of **_2 + 3 = 5_**. Use **_1_** based indexing for your response.  

**Note:**

- Two ice creams having unique IDs **_i_** and **_j_** may have the same cost (i.e., **_cost[i]_** ≡ **_cost[j]_**).  
- There will always be a unique solution.  

<h4>Function Description</h4>

Complete the function `whatFlavors`. It must determine the two flavors they will purchase and print them as two space-separated integers on a line.  

`whatFlavors` has the following parameter(s):  

- `cost`: an array of integers representing price for a flavor  
- `money`: an integer representing the amount of money they have to spend  

<h4>Input Format</h4>

The first line contains an integer, **_t_**, the number of trips to the ice cream parlor.  

Each of the next **_t_** sets of **_3_** lines is as follows:  

- The first line contains **_money_**.  
- The second line contains an integer, **_n_**, the size of the array **_cost_**.  
- The third line contains **_n_** space-separated integers denoting the **_cost[i]_**.  

<h4>Constraints</h4>

- **_1 ≤ t ≤ 50_**  
- **_2 ≤ money ≤ 10<sup>9</sup>_**  
- **_2 ≤ n ≤ 5 · 10<sup>4</sup>_**  
- **_1 ≤ cost[i] ≤ 10<sup>9</sup>_**  

<h4>Output Format</h4>

Print two space-separated integers denoting the respective indices for the two distinct flavors they choose to purchase in ascending order. Recall that each ice cream flavor has a unique ID number in the inclusive range from **_1_** to **_|cost|_**.  

<h4>Sample Input</h4>

```
2
4
5
1 4 5 3 2
4
4
2 2 4 3
```

<h4>Sample Output</h4>

```
1 4
1 2
```

<h4>Explanation</h4>

Sunny and Johnny make the following two trips to the parlor:  

1. The first time, they pool together **_money = 4_** dollars. There are five flavors available that day and flavors **_1_** and **_4_** have a total cost of **_1 + 3 = 4_**.  
2. The second time, they pool together **_money = 4_** dollars. There are four flavors available that day and flavors **_1_** and **_2_** have a total cost of **_2 + 2 = 4_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Ice_Cream_Parlor.cpp -o Ice_Cream_Parlor
```

This program can be run by calling:  

```
./Ice_Cream_Parlor
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Ice_Cream_Parlor
```
