#include <bits/stdc++.h>

using namespace std;

struct data {
  int oldIndex,
      value;
};

void whatFlavors(vector<int>, int);
void heapSort(data [], int);
void heapify(data [], int, int);
vector<string> split_string(string);

int main() {
  int t;
  cin >> t;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int t_itr = 0; t_itr < t; t_itr ++) {
    int money;
    cin >> money;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    string cost_temp_temp;
    getline(cin, cost_temp_temp);
    
    vector<string> cost_temp = split_string(cost_temp_temp);
    vector<int> cost(n);
    
    for (int i = 0; i < n; i ++) {
      int cost_item = stoi(cost_temp[i]);
      cost[i] = cost_item;
    }
    
    whatFlavors(cost, money);
  }
  return 0;
}

void whatFlavors(vector<int> cost, int money) {
  // local variables
  int size = cost.size();
  data values[size];
  
  for (int i = 0; i < size; i ++) {
    data d;
    d.value = cost[i];
    d.oldIndex = i;
    values[i] = d;
  }
  heapSort(values, size);

  int leftIndex = 0,
      rightIndex = size - 1,
      sum = 0;
  
  while (leftIndex != rightIndex) {
    sum = values[leftIndex].value + values[rightIndex].value;
    
    if (sum == money) {
      if (values[leftIndex].oldIndex < values[rightIndex].oldIndex)
        cout << values[leftIndex].oldIndex + 1 << " " << values[rightIndex].oldIndex + 1 << endl;
      else
        cout << values[rightIndex].oldIndex + 1 << " " << values[leftIndex].oldIndex + 1 << endl;
      return;
    } else if (sum < money) {
      leftIndex ++;
      
    } else {
      rightIndex --;
      
    }
  }
  cout << "well fuck" << endl;
  return;
}

void heapSort(data values[], int n) {
  for (int i = n / 2 - 1; i >= 0; i --)
    heapify(values, n, i);
  
  for (int i = n - 1; i >= 0; i --) {
    data d = values[0];
    values[0] = values[i];
    values[i] = d;
    heapify(values, i, 0);
  }
}

void heapify(data values[], int n, int i) {
  // local variables
  int largest = i,
      l = 2 * i + 1,
      r = l +1;
      
  if (l < n && values[l].value > values[largest].value)
    largest = l;
    
  if (r < n && values[r].value > values[largest].value)
    largest = r;
  
  if (largest != i) {
    data d = values[i];
    values[i] = values[largest];
    values[largest] = d;
    heapify(values, n, largest);
  }
}

vector<string> split_string(string input_string) {
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [](const char &x, const char &y) {
    return x == y and x ==' ';
  });
  
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == ' ')
    input_string.pop_back();
  
  vector<string> splits;
  char delimiter = ' ';
  size_t i = 0,
         pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
