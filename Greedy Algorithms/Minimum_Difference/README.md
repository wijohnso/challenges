<h3>Minimum Absolute Difference in an Array</h3>

The absolute difference is the positive difference between two values **_a_** and **_b_**, is written **_|a - b|_** or **_|b - a|_** and they are equal. If **_a = 3_** and **_b = 2_**, **_|3 - 2| = |2 - 3| = 1_**. Given an array of integers, find the minimum absolute difference between any two elements in the array.  

**Example**. There are **_3_** pairs of numbers: **_[-2, 2]_**, **_[-2,4]_** and **_[2,4]_**. The absolute differences for these pairs are **_|(-2) - 2| = 4_**, **_|(-2) - 4| = 6_** and **_|2 - 4| = 2_**. The minimum absolute difference is **_2_**.  

<h4>Function Description</h4>

Complete the `minimumAbsoluteDifference` function. It should return an integer that represents the minimum absolute difference between any pair of elements.  

`minimumAbsoluteDifference` has the following parameter(s):  

- `int arr[n]`: an array of integers  

<h4>Returns</h4>

- `int`: the minimum absolute difference found  

<h4>Input Format</h4>

The first line contains a single integer **_n_**, the size of **_arr_**.  
The second line contains **_n_** space-separated integers, **_arr[i]_**.  

<h4>Constraints</h4>

- **_2 ≤ n ≤ 10<sup>5</sup>_**  
- **_-10<sup>9</sup> ≤ arr[i] 10<sup>9</sup>_**  

<h4>Sample Input 0</h4>

```
3
3 -7 0
```

<h4>Sample Output 0</h4>

```
3
```

<h4>Explanation 0</h4>

The first line of input is the number of array elements. The array, **_arr = [3,-7,0]_**. There are three pairs to test: **_(3,7)_**, **_(3,0)_**, and **_(-7,0)_**. The absolute differences are:  

- **_|3 - (-7)| = 10_**  
- **_|3 - 0| = 3_**  
- **_|(-7) - 0| = 7_**  

Remember that the order of values in the subtraction does not influence the result. The smallest of these absolute differences is **_3_**.  

<h4>Sample Input 1</h4>

```
10
-59 -36 -13 1 -53 -92 -2 -96 -54 75
```

<h4>Sample Output 1</h4>

```
1
```

<h4>Explanation 1</h4>

The smallest absolute difference is **_|(-54) - (-53)| = 1_**.  

<h4>Sample Input 2</h4>

```
5
1 -3 71 68 17
```

<h4>Sample Output 2</h4>

```
3
```

<h4>Explanation 2</h4>

The minimum absolute difference is **_|71 - 68| = 3_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Minimum_Difference.cpp -o Minimum_Difference
```

This program can be run by calling:  

```
./Minimum_Difference
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Minimum_Difference
```
