<h3>Max Min</h3>

You will be given a list of integers, **_arr_**, and a single integer **_k_**. You must create an array of length **_k_** from elements of **_arr_** such that its unfairness is minimized. Call that array **_arr'_**. Unfairness of an array is calculated as  

<div align="center">
<b><i>max(arr') - min(arr')</i></b>
</div>

Where:  
- max denotes the largest integer in **_arr'_**  
- min denotes the smallest integer in **_arr'_**  

<h4>Example</h4>

**_arr = [1,4,7,2]_**  
**_k = 2_**  

Pick any two elements, say **_arr' = [4,7]_**.  
**_unfairness = max(4,7) - min(4,7) = 7 - 4 = 3_**  
Testing for all pairs, the solution **_[1,2]_** provides the minimum unfairness.  

**Note**: Integers in **_arr_** may not be unique.  

<h4>Function Description</h4>

Complete the `maxMin` function.  
`maxMin` has the following parameter(s):  

- `int k`: the number of elements to select  
- `int arr[n]`: an array of integers  

<h4>Returns</h4>

- `int`: the minimum possible unfairness  

<h4>Input Format</h4>

The first line contains an integer **_n_**, the number of elements in array **_arr_**.  
The second line contains an integer **_k_**.  
Each of the next **_n_** lines contains an integer **_arr[i]_** where **_0 ≤ i < n_**.  

<h4>Constraints</h4>

- **_2 ≤ n ≤ 10<sup>5</sup>_**  
- **_2 ≤ k ≤ n_**  
- **_0 ≤ arr[i] ≤ 10<sup>9</sup>_**  

<h4>Sample Input 0</h4>

```
7
3
10
100
300
200
1000
20
30
```

<h4>Sample Output 0</h4>

```
20
```

<h4>Explanation 0</h4>

Here **_k = 3_**; selecting the **_3_** integers **_10,20,30_**, unfairness equals  

```
max(10,20,30) - min(10,20,30) = 30 - 10 = 20
```

<h4>Sample Input 1</h4>

```
10
4
1
2
3
4
10
20
30
40
100
200
```

<h4>Sample Output 1</h4>

```
3
```

<h4>Explanation 1</h4>

Here **_k = 4_**; selecting the **_4_** integers **_1,2,3,4_**, unfairness equals  

```
max(1,2,3,4) - min(1,2,3,4) = 4 - 1 = 3
```

<h4>Sample Input 2</h4>

```
5
2
1
2
1
2
1
```

<h4>Sample Output 2</h4>

```
0
```

<h4>Explanation 2</h4>

Here **_k = 2_**. **_arr' = [2,2]_** or **_arr' = [1,2]_** give the minimum unfairness of **_0_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Max_Min.cpp -o Max_Min
```

This program can be run by calling:  

```
./Max_Min
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Max_Min
```
