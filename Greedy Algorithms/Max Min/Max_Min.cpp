#include <bits/stdc++.h>

using namespace std;

int maxMin(int, vector<int>);

int main() {
  int n;
  cin >> n;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  int k;
  cin >> k;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  vector<int> arr(n);
  
  for (int i = 0; i < n; i ++) {
    int arr_item;
    cin >> arr_item;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    arr[i] = arr_item;
  }
  
  int result = maxMin(k, arr);
  
  cout << result << endl;
  
  return 0;
}

int maxMin(int k, vector<int> arr) {
  int returnVal,
      currentVal;
  
  sort(arr.begin(), arr.end());
  returnVal = arr[k - 1] - arr[0];
  
  for (int i = 1; i <= arr.size() - k; i ++)
    if ((currentVal = arr[i + k - 1] - arr[i]) < returnVal)
      returnVal = currentVal;
  
  return returnVal;
}
