<h3>Reverse Shuffle Merge</h3>

Given a string, **_A_**, we define some operations on the string as follows:  


a. **_reverse(A)_** denotes the string obtained by reversing string **_A_**. Example: **`reverse("abc") = "cba"`**  

b. **_shuffle(A)_** denotes any string that's a permutation of string **_A_**. Example: **`shuffle("god") ∈ ['god', 'gdo', 'ogd', 'odg', 'dgo', 'dog']`**  



c. **_merge(A1,A2)_** denotes any string that's obtained by interspersing the two strings **_A1_** & **_A2_**, maintaining the order of characters in both. For example, **`A1 = "abc"`** & **`A2 = "def"`**, one possible result of **_merge(A1,A2)_** could be **`"abcdef"`**, another could be **`"abdecf"`**, another could be **`"adbecf"`** and so on.  

Given a string **_s_** such **_s ∈ merge(reverse(A), shuffle(A))_** that for some string **_A_**, find the [lexicographically](https://en.wikipedia.org/wiki/Lexicographic_order) smallest **_A_**.  

For example, **_s = abab_**. We can split it into two strings of **_ab_**. The reverse is **_ba_** and we need to find a string to shuffle in to get **_abab_**. The middle two characters match our reverse string, leaving the **_a_** and **_b_** at the ends. Our shuffle string needs to be **_ab_**. Lexicographically **_ab < ba_**, so our answer is **_ab_**.  

<h4>Function Description</h4>

Complete the `reverseShuffleMerge` function. It must return the lexicographically smallest string fitting the criteria.  

`reverseShuffleMerge` has the following parameter(s):  

- `s`: a string  

<h4>Input Format</h4>

A single line containing the string **_s_**.  

<h4>Constraints</h4>

- **_s_** contains only lower-case English letters, ascii[a-z]  
- **_1 ≤ |s| ≤ 10000_**  

<h4>Output Format</h4>

Find and return the string which is the lexicographically smallest valid **_A_**.  

<h4>Sample Input 0</h4>

```
eggegg
```

<h4>Sample Output 0</h4>

```
egg
```

<h4>Explanation 0</h4>

Split "eggegg" into strings of like character counts: **_egg_** and **_egg_**  

reverse **_egg_** = **_gge_**  

shuffle **_egg_** can be **_egg_**  

**_eggegg_** belongs to the merge of **_gge_** and **_egg_**  

The merge is:e**gge**gg.  

'egg' < 'gge'  

<h4>Sample Input 1</h4>

```
abcdefgabcdefg
```

<h4>Sample Output 1</h4>

```
agfedcb
```

<h4>Explanation 1</h4>

Split the string into two strings with like characters: **_abcdefg_** and **_abcdefg_**.  

Reverse **_abcdefg_** = **_gfedcba_**  

Shuffle **_gfedcba_** can be **_bcdefga_**  

Merge to a**bcdefga**bcdefg  

<h4>Sample Input 2</h4>

```
aeiouuoiea
```

<h4>Sample Output 2</h4>

```
aeiou
```

<h4>Explanation 2</h4>

Split the string into groups of like characters: **_aeiou_**  

Reverse **_aeiou_** = **_uoiea_**  

These merge to aeiou**uoiea**  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Reverse_Shuffle_Merge.cpp -o Reverse_Shuffle_Merge
```

This program can be run by calling:  

```
./Reverse_Shuffle_Merge
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Reverse_Shuffle_Merge
```
