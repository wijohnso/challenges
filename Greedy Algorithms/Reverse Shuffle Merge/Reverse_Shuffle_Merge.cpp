#include <bits/stdc++.h>

#define SIZE 26

using namespace std;

string reverseShuffleMerge(string);

int main() {
  string s;
  getline(cin, s);
  
  string result = reverseShuffleMerge(s);
  
  cout << result << endl;
  
  return 0;
}

string reverseShuffleMerge(string s) {
  // local variables
  int n = s.length();
  vector<char> sArr(s.rbegin(), s.rend()),  // reverse s
               lex;
  vector<int> freq(SIZE, 0),
              didUse(SIZE, 0),
              canUse(SIZE, 0);
              
  for (int i = 0; i < n; i ++)
    freq[sArr[i] - 'a'] ++;
  for (int i = 0; i < SIZE; i ++) {
    canUse[i] = freq[i];
    freq[i] /= 2;
  }
  
  for (int i = 0; i < n; i ++) {
    // if one pair of letters hasn't been used
    if (didUse[sArr[i] - 'a'] < (freq[sArr[i] - 'a'])) {
      // find the lowest lexicographical beginning for lex by removing any larger letters from lex
      while ((lex.size() > 0) && 
             (sArr[i] < lex.back()) &&
             (didUse[lex.back() - 'a'] + canUse[lex.back() - 'a'] - 1 >= freq[lex.back() - 'a'])) {
        
        // remove the last letter in lex and adjust didUse count
        didUse[lex.back() - 'a'] --;
        lex.pop_back();
      }
      
      // add current letter to the back of lex and adjust canUse and didUse counts
      lex.push_back(sArr[i]);
      didUse[sArr[i] - 'a'] ++;
      canUse[sArr[i] - 'a'] --;
      
    // if this pair has already been used
    } else {
      canUse[sArr[i] - 'a'] --;
    }
  }
  
  return string(lex.begin(), lex.end());
}
