#include <bits/stdc++.h>

using namespace std;

int luckBalance(int, vector<vector<int>>);
vector<string> split_string(string);

int main() {
  string nk_temp;
  getline(cin, nk_temp);
  vector<string> nk = split_string(nk_temp);
  
  int n = stoi(nk[0]),
      k = stoi(nk[1]);
  
  vector<vector<int>> contests(n);
  
  for (int i = 0; i < n; i ++) {
    contests[i].resize(2);
    
    for (int j = 0; j < 2; j ++) {
      cin >> contests[i][j];
    }
    
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
  }
  
  int result = luckBalance(k, contests);
  
  cout << result << endl;
  
  return 0;
}

int luckBalance(int k, vector<vector<int>> contests) {
  // local variables
  vector<int> ones;
  int returnVal = 0,
      limit;

  for (int i = 0; i < contests.size(); i ++)
    if (contests[i][1])
      ones.push_back(contests[i][0]);
    else
      returnVal += contests[i][0];

  if (!ones.empty()) {
    sort(ones.begin(), ones.end());

    if (ones.size() > k)
      limit = ones.size() - k;
    else
      limit = 0;
    
    // losses
    for (int i = ones.size(); i > limit; i --) {
      cout << i << endl;
      returnVal += ones[i - 1];
    }
  
    if (ones.size() > k)
      // wins
      for (int i = 0; i < (ones.size() - k); i ++)
        returnVal -= ones[i];
    
  }

  return returnVal;
}

vector<string> split_string(string input_string) {
  char delimiter = ' ';
  
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [](const char &x, const char &y) {
    return x == y and x == ' ';
  
  });
  
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == delimiter)
    input_string.pop_back();
  
  vector<string> splits;
  
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
