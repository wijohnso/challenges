<h3>Luck Balance</h3>

Lena is preparing for an important coding competition that is preceded by a number of sequential preliminary contests. Initially, her luck balance is 0. She believes in "saving luck", and wants to check her theory. Each contest is described by two integers, **_L[i]_** and **_T[i]_**:  

- **_L[i]_** is the amount of luck associated with a contest. If Lena wins the contest, her luck balance will decrease by **_L[i]_**; if she loses it, her luck balance will increase by **_L[i]_**.  
- **_T[i]_** denotes the contest's importance rating. It's equal to **_1_** if the contest is important, and it's equal to **_0_** if it's unimportant.  

If Lena loses no more than **_k_** important contests, what is the maximum amount of luck she can have after competing in all the preliminary contests? This value may be negative.  

<h4>Example</h4>
**_k = 2_**  
**_L = [5,1,4]_**  
**_T = [1,2,0]_**  

```
Contest   L[i]  T[i]
1         5     1
2         1     1
3         4     0
```

If Lena loses all of the contests, her luck will be **_5 + 1 + 4 = 10)**. Since she is allowed to lose **_2_** important contests, and there are only **_2_** important contests, she can lose all three contests to maximize her luck at **_10_**.  

If **_k = 1_**, she has to win at least **_1_** of the **_2_** important contests. She would choose to win the lowest value important contest worth **_1_**. Her final luck will be **_5 + 4 - 1 = 8_**.  

<h4>Function Description</h4>

Complete the `luckBalance` function.  

`luckBalance` has the following parameter(s):  

- `int k`: the number of important contests Lena can lose.  
- `int contests[n][2]`: a 2D array of integers where each **_contests[i]_** contains two integers that represent the luck balance and importance of the **_i<sup>th</sup>_** contest.  

<h4>Returns</h4>

- `int`: the maximum luck balance achievable  

<h4>Input Format</h4>

The first line contains two space-separated integers **_n_** and **_k_**, the number of preliminary contests and the maximum number of important contests Lena can lose.  
Each of the next **_n_** lines contains two space-separated integers, **_L[i]_** and **_T[i]_**, the contest's luck balance and its importance rating.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 100_**  
- **_0 ≤ k ≤ N_**  
- **_1 ≤ L[i] ≤ 10<sup>4</sup>_**  
- **_T[i] ∈ {0,1}_**  

<h4>Sample Input</h4>

```
STDIN       Function
-----       --------
6 3         n = 6, k = 3
5 1         contests = [[5, 1], [2, 1], [1, 1], [8, 1], [10, 0], [5, 0]]
2 1
1 1
8 1
10 0
5 0
```

<h4>Sample Output

```
29
```

<h4>Explanation</h4>

There are **_n = 6_** contests. Of these contests, **_4_** are important and she cannot lose more than **_k = 3_** of them. Lena maximizes her luck if she wins the **_3<sup>rd</sup>_** important contest (where **_L[i] = 1_**) and loses all of the other five contests for a total luck balance of **_5 + 2 + 8 + 10 + 5 - 1 = 29_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Luck_Balance.cpp -o Luck_Balance
```

This program can be run by calling:  

```
./Luck_Balance
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Luck_Balance
```
