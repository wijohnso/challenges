#include <bits/stdc++.h>

using namespace std;

int getMinimumCost(int, vector<int>);
vector<string> split_string(string);

int main() {
  string nk_temp;
  getline(cin, nk_temp);
  vector<string> nk = split_string(nk_temp);
  
  int n = stoi(nk[0]),
      k = stoi(nk[1]);
      
  string c_temp_temp;
  getline(cin, c_temp_temp);
  vector<string> c_temp = split_string(c_temp_temp);
  
  vector<int> c(n);
  
  for (int i = 0; i < n; i ++) {
    int c_item = stoi(c_temp[i]);
    c[i] = c_item;
  }
  
  int minimumCost = getMinimumCost(k, c);
  
  cout << minimumCost << endl;
  
  return 0;
}

int getMinimumCost(int k, vector<int> c) {
  int flowersPerFriend = ceil(c.size() / (double)k),
      i = c.size(),
      returnVal = 0;
  
  sort(c.begin(), c.end());
      
  for (int m = 0; m < flowersPerFriend; m ++) {
    for (int f = 0; f < k; f ++) {
      if (--i < 0)
        return returnVal;
      
      returnVal += (m + 1) * c[i];
    }
  }
      
  return returnVal;
}

vector<string> split_string(string input_string) {
  char delimiter = ' ';
  
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [](const char &x, const char &y) {
    return x == y and x == ' ';
  
  });
  
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == delimiter)
    input_string.pop_back();
  
  vector<string> splits;
  
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
