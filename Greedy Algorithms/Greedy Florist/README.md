<h3>Greedy Florist</h3>

A group of friends want to buy a bouquet of flowers. The florist wants to maximize his number of new customers and the money he makes. To do this, he decides he'll multiply the price of each flower by the number of that customer's previously purchased flowers plus **_1_**. The first flower will be original price, **_(0 + 1) ⋅ original price_**, the next will be **_(1 + 1) ⋅ original price_** and so on.  

Given the size of the group of friends, the number of flowers they want to purchase and the original prices of the flowers, determine the minimum cost to purchase all of the flowers.  

For example, if there are **_k = 3_** friends that want to buy **_n = 4_** flowers that cost **_c = [1,2,3,4]_**, each friend will buy one of the flowers priced **_[2,3,4]_** at the original price. Having each purchased **_x = 1_** flower, the first flower in the list, **_c[0]_**, will now cost **_(current purchase + previous purchase) ⋅ c[0] = (1 + 1) ⋅ 1 = 2_**. The total cost will be **_2 + 3 + 4 + 2 = 11_**.  

<h4>Function Description</h4>

Complete the `getMinimumCost` function. It should return the minimum cost to purchase all of the flowers.  

`getMinimumCost` has the following parameter(s):  

- `c`: an array of integers representing the original price of each flower  
- `k`: an integer, the number of friends  

<h4>Input Format</h4>

The first line contains two space-separated integers **_n_** and **_k_**, the number of flowers and the number of friends.  
The second line contains **_n_** space-separated positive integers **_c[i]_**, the original price of each flower.  

<h4>Constraints</h4>

- **_1 ≤ n,k ≤ 100_**  
- **_1` ≤ c[i] ≤ 10<sup>6</sup>_**  
- **_answer < 2<sup>31</sup>_**  
- **_0 ≤ i < n_**  

<h4>Output Format</h4>

Print the minimum cost to buy all **_n_** flowers.  
<h4>Sample Input 0</h4>

```
3 3
2 5 6
```

<h4>Sample Output 0</h4>

```
13
```

<h4>Explanation 0</h4>

There are **_n = 3_** flowers with costs **_c = [2,5,6]_** and **_k = 3_** people in the group. If each person buys one flower, the total cost of prices paid is **_2 + 5 + 6 = 13_** dollars. Thus, we print **_13_** as our answer.  

<h4>Sample Input 1</h4>

```
3 2
2 5 6
```

<h4>Sample Output 1</h4>

```
15
```

<h4>Explanation 1</h4>

There are **_n = 3_** flowers with costs **_c = [2,5,6]_** and **_k = 2_** people in the group. We can minimize the total purchase cost like so:  

1. The first person purchases **_2_** flowers in order of decreasing price; this means they buy the more expensive flower (**_c<sub>1</sub> = 5_**) first at price **_p<sub>1</sub> = (0 + 1) ⋅ 5 = 5_** dollars and the less expensive flower (**_c<sub>0</sub> = 2_**) second at price **_p<sub>0</sub> = (1 + 1) ⋅ 2 = 4_** dollars.  
2. The second person buys the most expensive flower at price **_p<sub>2</sub> = (0 + 1) ⋅ 6 = 6_** dollars.  

We then print the sum of these purchases, which is **_5 + 4 + 6 = 15_**, as our answer.  

<h4>Sample Input 2</h4>

```
5 3
1 3 5 7 9
```

<h4>Sample Output 2</h4>

```
29
```

<h4>Explanation 2</h4>

The friends buy flowers for **_9_**, **_7_** and **_3_**, **_5_** and **_1_** for a cost of **_9 + 7 + 3 ⋅ (1 + 1) + 5 + 1 ⋅ (1 + 1) = 29_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Greedy_Florist.cpp -o Greedy_Florist
```

This program can be run by calling:  

```
./Greedy_Florist
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Greedy_Florist
```
