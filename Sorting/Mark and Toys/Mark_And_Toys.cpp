#include <bits/stdc++.h>

using namespace std;

int maximumToys(vector<int>, int);
vector<string> split_string(string);

int main() {
  string nk_temp;
  getline(cin, nk_temp);
  
  vector<string> nk = split_string(nk_temp);
  
  int n = stoi(nk[0]),
      k = stoi(nk[1]);
      
  string prices_temp_temp;
  getline(cin, prices_temp_temp);
  
  vector<string> prices_temp = split_string(prices_temp_temp);
  
  vector<int> prices(n);
  
  for (int i = 0; i < n; i ++) {
    int prices_item = stoi(prices_temp[i]);
    prices[i] = prices_item;
  }
  
  int result = maximumToys(prices, k);
  
  cout << result << endl;
  
  return 0;
}

int maximumToys(vector<int> prices, int k) {
  // local variables
  int numToys = 0;
  sort(prices.begin(), prices.end());
  
  for (int i = 0; i < prices.size() && k > 0; i ++) {
    k -= prices[i];
    if (k > 0)
      numToys ++;
  }
  
  return numToys;
}

vector<string> split_string(string input_string) {
  char delimiter = ' ';
  
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [](const char &x, const char &y) {
    return x == y and x == ' ';
  
  });
  
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == delimiter)
    input_string.pop_back();
  
  vector<string> splits;
  
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
