<h3>Mark and Toys</h3>

Mark and Jane are very happy after having their first child. Their son loves toys, so Mark wants to buy some. There are a number of different toys lying in front of him, tagged with their prices. Mark has only a certain amount to spend, and he wants to maximize the number of toys he buys with this money. Given a list of toy prices and an amount to spend, determine the maximum number of gifts he can buy.  

**Note** Each toy can be purchased only once.  

<h4>Example</h4>

**_prices = [1,2,3,4]_**  
**_k = 7_**  

The budget is **_7_** units of currency. He can buy items that cost **_[1,2,3]_** for **_6_**, or **_[3,4]_** for **_7_** units. The maximum is **_3_** items.  

<h4>Function Description</h4>

Complete the function `maximumToys`.  

`maximumToys` has the following parameter(s):  

- `int prices[n]`: the toy prices  
- `int k`: Mark's budget  

<h4>Returns</h4>

- `int`: the maximum number of toys  

<h4>Input Format</h4>

The first line contains two integers, **_n_** and **_k_**, the number of priced toys and the amount Mark has to spend.  
The next line contains **_n_** space-separated integers **_prices[i]_**.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 10<sup>5</sup>_**  
- **_1 ≤ k ≤ 10<sup>9</sup>_**  
- **_1 ≤ prices[i] ≤ 10<sup>9</sup>_**  

A toy can't be bought multiple times.

<h4>Sample Input</h4>

```
7 50
1 12 5 111 200 1000 10
```

<h4>Sample Output</h4>

```
4
```

<h4>Explanation</h4>

He can buy only **_4_** toys at most. These toys have the following prices: **_1, 12, 5, 10_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Mark_And_Toys.cpp -o Mark_And_Toys
```

This program can be run by calling:  

```
./Mark_And_Toys
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Mark_And_Toys
```
