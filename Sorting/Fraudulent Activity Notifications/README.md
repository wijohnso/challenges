<h3>Fraudulent Activity Notifications</h3>

HackerLand National Bank has a simple policy for warning clients about possible fraudulent account activity. If the amount spent by a client on a particular day is greater than or equal to **_2 x_** the client's median spending for a trailing number of days, they send the client a notification about potential fraud. The bank doesn't send the client any notifications until they have at least that trailing number of prior days' transaction data.  

Given the number of trailing days **_d_** and a client's total daily expenditures for a period of **_n_** days, find and print the number of times the client will receive a notification over all **_n_** days.  

For example, **_d = 3_** and **_expeniatures = [10,20,30,40,50]_**. On the first three days, they just collect spending data. At day **_4_**, we have trailing expenditures of **_[10,20,30]_**. The median is **_20_** and the day's expenditure is **_40_**. Because **_40 ≥ 2 · 20_**, there will be a notice. The next day, our trailing expenditures are **_[20,30,40]_** and the expenditures are **_50_**. This is less than **_2 · 30_** so no notice will be sent. Over the period, there was one notice sent.  

**Note**: The median of a list of numbers can be found by arranging all the numbers from smallest to greatest. If there is an odd number of numbers, the middle one is picked. If there is an even number of numbers, median is then defined to be the average of the two middle values.

<h4>Function Description</h4>

Complete the function `activityNotifications`. It must return an integer representing the number of client notifications.  

`activityNotifications` has the following parameter(s):  

- `expenditure`: an array of integers representing daily expenditures  
- `d`: an integer, the lookback days for median spending  

<h4>Input Format</h4>

The first line contains two space-separated integers **_n_** and **_d_**, the number of days of transaction data, and the number of trailing days' data used to calculate median spending.  
The second line contains **_n_** space-separated non-negative integers where each integer **_i_** denotes **_expenditure[i]_**.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 2 · 10<sup>5</sup>_**  
- **_1 ≤ d ≤ n_**  
- 0 ≤ expenditure[i] ≤ 200_**  

<h4>Output Format</h4>

Print an integer denoting the total number of times the client receives a notification over a period of **_n_** days.  

<h4>Sample Input 0</h4>

```
9 5
2 3 4 2 3 6 8 4 5
```

<h4>Sample Output 0</h4>

```
2
```

<h4>Explanation 0</h4>

We must determine the total number of **_notifications_** the client receives over a period of **_n = 9_** days. For the first five days, the customer receives no notifications because the bank has insufficient transaction data: **_notifications = 0_**.  

On the sixth day, the bank has **_d = 5_** days of prior transaction data, **_{2,3,4,2,3}_**, and **_median = 3_** dollars. The client spends **_6_** dollars, which triggers a notification because **_6 ≥ 2 · median_**: **_notifications = 0 + 1 = 1_**.  

On the seventh day, the bank has **_d = 5_** days of prior transaction data, **_{3,4,2,3,6}_**, and **_median = 3_** dollars. The client spends **_8_** dollars, which triggers a notification because **_8 ≥ 2 · median_**: **_notifications = 1 + 1 = 2_**.  

On the eighth day, the bank has **_d = 5_** days of prior transaction data, **_{4,2,3,6,8}_**, and **_median = 4_** dollars. The client spends**_4_**  dollars, which does not trigger a notification because **_4 < 2 · median_**: **_notifications = 2 + 0 = 2_**.  

On the ninth day, the bank has **_d = 5_** days of prior transaction data, **_{2,3,6,8,4}_**, and a transaction median of **_4_** dollars. The client spends **_5_** dollars, which does not trigger a notification because **_5 < 2 · median_**: **_notifications = 2 + 0 = 2_**.  

<h4>Sample Input 1</h4>

```
5 4
1 2 3 4 4
```

<h4>Sample Output 1</h4>

```
0
```

There are **_4_** days of data required so the first day a notice might go out is day **_5_**. Our trailing expenditures are **_[1,2,3,4]_** with a median of **_2.5_**. The client spends **_4_** which is less than **_2 · 2.5_** so no notification is sent.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Fraudulent_Activity_Notifications.cpp -o Fraudulent_Activity_Notifications
```

This program can be run by calling:  

```
./Fraudulent_Activity_Notifications
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Fraudulent_Activity_Notifications
```
