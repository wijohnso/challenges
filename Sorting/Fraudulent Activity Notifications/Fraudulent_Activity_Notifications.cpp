#include <bits/stdc++.h>

using namespace std;

int activityNotifications(vector<int>, int);
vector<string> split_string(string);

int main() {
  string nd_temp;
  getline(cin, nd_temp);
  
  vector<string> nd = split_string(nd_temp);
  
  int n = stoi(nd[0]),
      d = stoi(nd[1]);
      
  string expenditure_temp_temp;
  getline(cin, expenditure_temp_temp);
  
  vector<string> expenditure_temp = split_string(expenditure_temp_temp);
  
  vector<int> expenditure(n);
  
  for (int i = 0; i < n; i ++) {
    int expenditure_item = stoi(expenditure_temp[i]);
    expenditure[i] = expenditure_item;
  }
  
  int result = activityNotifications(expenditure, d);
  
  cout << result << endl;
  
  return 0;
}

int activityNotifications(vector<int> expenditure, int d) {
  // local variables
  int size = expenditure.size(),
      median,
      notifications = 0;
  bool isOdd = d % 2 == 0 ? false : true;
  vector<int> trailing(201);
  
  // add the first d valuse to the trailing vector
  for (int i = 0; i < d; i ++)
    trailing[expenditure[i]] ++;
  
  // for all complete sets of trailing values
  for (int i = d; i < size; i ++) {
    // copy the trailing vector
    vector<int> trailing2(trailing);
    
    // create indices to determine where the median value is
    for (int j = 1; j < 201; j ++)
      trailing2[j] += trailing2[j - 1];
    
    // odd is the average of the two middle indices
    if (!isOdd) {
      int index = floor(d / 2);
      int j = 0;
      while (trailing2[j] < index)
        j ++;
      median = j;
      while (trailing2[j] < index + 1)
        j ++;
      median += j;
      
    // even is the middle index
    } else {
      int index = ceil(d / 2.0);
      int j = 0;
      while (trailing2[j] < index)
        j ++;
      median = 2 * j;
    }
    
    if (median <= expenditure[i])
      notifications ++;
    
    // eliminate the oldest value and add a new value to the trailing vector
    trailing[expenditure[i - d]] --;
    trailing[expenditure[i]] ++;
  }
  
  return notifications;
}

vector<string> split_string(string input_string) {
  char delimiter = ' ';
  
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [](const char &x, const char &y) {
    return x == y and x == ' ';
  
  });
  
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == delimiter)
    input_string.pop_back();
  
  vector<string> splits;
  
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
