#include <bits/stdc++.h>

using namespace std;

long countInversions(vector<int> &);
vector<string> split_string(string);

int main() {
  int t;
  cin >> t;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int t_itr = 0; t_itr < t; t_itr ++) {
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    string arr_temp_temp;
    getline(cin, arr_temp_temp);
    
    vector<string> arr_temp = split_string(arr_temp_temp);
    
    vector<int> arr(n);
    
    for (int i = 0; i < n; i ++) {
      int arr_item = stoi(arr_temp[i]);
      arr[i] = arr_item;
    }
    
    long result = countInversions(arr);
    
    cout << result << endl;
  }
}

long countInversions(vector<int> &arr) {
  if (arr.size() == 1) {
    return 0;
    
  } else {
    int size = ceil(arr.size() / 2.0);
    vector<int>::iterator it = arr.begin();
    vector<int> left(it, it + size),
                right(it + size, it + arr.size());
    long swaps = countInversions(left) + countInversions(right);

    int l = 0,
        r = 0;
    for (int i = 0; i < arr.size(); i ++) {
      if (l == left.size()) {
        arr[i] = right[r];
        r ++;
        
      } else if (r == right.size()) {
        arr[i] = left[l];
        l ++;
        
      } else if (right[r] < left[l]) {
        swaps += (left.size() - l);
        arr[i] = right[r];
        r ++;
        
      } else {
        arr[i] = left[l];
        l ++;
      }
    }
    
    return swaps;
  }
}

vector<string> split_string(string input_string) {
  char delimiter = ' ';
  
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [](const char &x, const char &y) {
    return x == y and x == ' ';
  
  });
  
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == delimiter)
    input_string.pop_back();
  
  vector<string> splits;
  
  size_t i = 0,
  pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
