<h3>Merge Sort: Counting Inversions</h3>

In an array, **_arr_**, the elements at indices **_i_** and **_j_** (where **_i < j_**) form an inversion if **_arr[i] > arr[j]_**. In other words, inverted elements **_arr[i]_** and **_arr[j]_** are considered to be "out of order". To correct an inversion, we can swap adjacent elements.  

<h4>Example</h4>

**_arr = [2,4,1]_**  

To sort the array, we must perform the following two swaps to correct the inversions:  

**_arr = [2,4,1] → swap(arr[1],arr[2]) → swap(arr[0],arr[1]) → [1,2,4]_**  

The sort has two inversions: **_(4,1)_** and **_(2,1)_**.  

Given an array **_arr_**, return the number of inversions to sort the array.  

<h4>Function Description</h4>

Complete the function `countInversions`.  

`countInversions` has the following parameter(s):  

- `int arr[n]`: an array of integers to sort  

<h4>Returns</h4>

- `int`: the number of inversions  

<h4>Input Format</h4>

The first line contains an integer, **_d_**, the number of datasets.  

Each of the next **_d_** pairs of lines is as follows:  

1. The first line contains an integer, **_n_**, the number of elements in **_arr_**.  
2. The second line contains **_n_** space-separated integers, **_arr[i]_**.  

<h4>Constraints</h4>

- **_1 ≤ d ≤ 15_**  
- **_1 ≤ n ≤ 10<sup>5</sup>_**  
- **_1 ≤ arr[i] ≤ 10<sup>7</sup>_**  

<h4>Sample Input</h4>

```
STDIN       Function
-----       --------
2           d = 2
5           arr[] size n = 5 for the first dataset
1 1 1 2 2   arr = [1, 1, 1, 2, 2]
5           arr[] size n = 5 for the second dataset     
2 1 3 1 2   arr = [2, 1, 3, 1, 2]
```

<h4>Sample Output</h4>

```
0  
4   
```

<h4>Explanation</h4>

We sort the following **_d = 2_** datasets:  

1. **_arr = [1,1,1,2,2]_** is already sorted, so there are no inversions for us to correct.  
2. **_arr = [2,1,3,1,2] → [1,2,3,1,2]_** (1 swap) **_→ [1,1,2,3,2]_** (2 swaps) **_→ [1,1,2,2,3]_** (1 swap)  
   We performed a total of **_1 + 2 + 1 = 4_** swaps to correct inversions.  
   
<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Counting_Inversions.cpp -o Counting_Inversions
```

This program can be run by calling:  

```
./Counting_Inversions
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Counting_Inversions
```
