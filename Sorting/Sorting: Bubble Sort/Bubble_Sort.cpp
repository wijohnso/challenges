#include <bits/stdc++.h>

using namespace std;

void countSwaps(vector<int>);
vector<string> split_string(string);

int main() {
  int n;
  cin >> n;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  string a_temp_temp;
  getline(cin, a_temp_temp);
  
  vector<string> a_temp = split_string(a_temp_temp);
  
  vector<int> a(n);
  
  for (int i = 0; i < n; i ++) {
    int a_item = stoi(a_temp[i]);
    a[i] = a_item;
  }
  
  countSwaps(a);
  
  return 0;
}

void countSwaps(vector<int> a) {
  // local variables
  int n = a.size(),
      swaps = 0;
  
  for (int i = 0; i < n; i ++) {
    for (int j = 0; j < n - 1; j ++) {
      if (a[j] > a[j + 1]) {
        swap(a[j], a[j + 1]);
        swaps ++;
      }
    }
  }
  
  cout << "Array is sorted in " << swaps << " swaps." << endl
       << "First Element: " << a[0] << endl
       << "Last Element: " << a[a.size() - 1] << endl;
  
  return;
}

vector<string> split_string(string input_string) {
  char delimiter = ' ';
  
  string::iterator new_end = unique(input_string.begin(), input_string.end(), [](const char &x, const char &y) {
    return x == y and x == ' ';
    
  });
  
  input_string.erase(new_end, input_string.end());
  
  while (input_string[input_string.length() - 1] == delimiter)
    input_string.pop_back();
  
  vector<string> splits;
  
  size_t i = 0,
         pos = input_string.find(delimiter);
  
  while (pos != string::npos) {
    splits.push_back(input_string.substr(i, pos - i));
    i = pos + 1;
    pos = input_string.find(delimiter, i);
  }
  
  splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));
  
  return splits;
}
