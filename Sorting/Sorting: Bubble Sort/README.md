<h3>Sorting: Bubble Sort</h3>

Consider the following version of Bubble Sort:  

```
for (int i = 0; i < n; i++) {
    for (int j = 0; j < n - 1; j++) {
        // Swap adjacent elements if they are in decreasing order
        if (a[j] > a[j + 1]) {
            swap(a[j], a[j + 1]);
        }
    }
}
```

Given an array of integers, sort the array in ascending order using the Bubble Sort algorithm above. Once sorted, print the following three lines:  

1. `Array is sorted in numSwaps swaps`., where **_numSwaps_** is the number of swaps that took place.  
2. `First Element: firstElement`, where **_firstElement_**  is the first element in the sorted array.  
3. `Last Element: lastElement`, where **_lastElement_** is the last element in the sorted array.  

**Hint**: To complete this challenge, you must add a variable that keeps a running tally of all swaps that occur during execution.

<h4>Example</h4>

**_a = [6,4,1]_**  

```
swap    a       
0       [6,4,1]
1       [4,6,1]
2       [4,1,6]
3       [1,4,6]
```

The steps of the bubble sort are shown above. It took **_3_** swaps to sort the array. Output is:  

```
Array is sorted in 3 swaps.  
First Element: 1  
Last Element: 6  
```

<h4>Function Description</h4>

Complete the function `countSwaps`.  

`countSwaps` has the following parameter(s):  

- `int a[n]`: an array of integers to sort  

<h4>Prints</h4>

Print the three lines required, then return. No return value is expected.  

<h4>Input Format</h4>

The first line contains an integer, **_n_**, the size of the array **_a_**.  
The second line contains **_n_** space-separated integers **_a[i]_**.  

<h4>Constraints</h4>

- **_2 ≤ n ≤ 600_**  
- **_1 ≤ a[i] ≤ 2·10<sup>6</sup>_**  

<h4>Sample Input 0</h4>

```
STDIN   Function
-----   --------
3       a[] size n = 3
1 2 3   a = [1, 2, 3]
```

<h4>Sample Output 0</h4>

```
Array is sorted in 0 swaps.
First Element: 1
Last Element: 3
```

<h4>Explanation 0</h4>

The array is already sorted, so **_0_** swaps take place.  

<h4>Sample Input 1</h4>

```
3
3 2 1
```

<h4>Sample Output 1</h4>

```
Array is sorted in 3 swaps.
First Element: 1
Last Element: 3
```

<h4>Explanation 1</h4>
The array is not sorted, and its initial values are: **_{3,2,1}_**. The following **_3_** swaps take place:  

1. **_{3,2,1} → {2,3,1}_**  
2. **_{2,3,1} → {2,1,3}_**  
3. **_{2,1,3} → {1,2,3}_**  

At this point the array is sorted and the three lines of output are printed to stdout.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Bubble_Sort.cpp -o Bubble_Sort
```

This program can be run by calling:  

```
./Bubble_Sort
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Bubble_Sort
```
