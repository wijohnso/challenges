<h3>Sorting: Comparator</h3>

Comparators are used to compare two objects. In this challenge, you'll create a comparator and use it to sort an array. The `Player` class is provided. It has two fields:  

1. `name`: a string.  
2. `score`: an integer.  

Given an array of **_n_** `Player` objects, write a comparator that sorts them in order of decreasing score. If **_2_** or more players have the same score, sort those players alphabetically ascending by name. To do this, you must create a `Checker` class that implements the Comparator interface, then write an `int compare(Player a, Player b)` method implementing the `Comparator.compare(T o1, T o2)` method. In short, when sorting in ascending order, a comparator function returns **_-1_** if **_a < b_**, **_0_** if **_a = b_**, and **_1_** if **_a > b_**.  

Declare a `Checker` class that implements the comparator method as described. It should sort first descending by score, then ascending by name. The code stub reads the input, creates a list of `Player` objects, uses your method to sort the data, and prints it out properly.  

<h4>Example</h4>

**_n = 3_**  
**_data = [[smith,20],[jones,15],[jones,20]]_**  

Sort the list as **_data<sub>sorted</sub> = [[jones,20],[smith,20],[jones,15]]_**. Sort first descending by score, then ascending by name.  

<h4>Input Format</h4>

The first line contains an integer, **_n_**, the number of players.  
Each of the next **_n_** lines contains a player's **_name_** and **_score_**, a string and an integer.  

<h4>Constraints</h4>

- **_0 ≤ score ≤ 1000_**  
- Two or more players can have the same name.  
- Player names consist of lowercase English alphabetic letters.  

<h4>Output Format</h4>

You are not responsible for printing any output to `stdout`. Locked stub code in Solution will instantiate a `Checker` object, use it to sort the `Player` array, and print each sorted element.  

<h4>Sample Input</h4>

```
5
amy 100
david 100
heraldo 50
aakansha 75
aleksa 150
```

<h4>Sample Output</h4>

```
aleksa 150
amy 100
david 100
aakansha 75
heraldo 50
```

<h4>Explanation</h4>

The players are first sorted descending by score, then ascending by name.

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Comparator.cpp -o Comparator
```

This program can be run by calling:  

```
./Comparator
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Comparator
```
