<h3>Inserting a Node Into a Doubly Linked List</h3>

Given a reference to the head of a doubly-linked list and an integer, **_data_**, create a new `DoublyLinkedListNode` object having data value **_data_** and insert it at the proper location to maintain the sort.  

<h4>Example</h4>

**_head_** refers to the list **_1 ⟷ 2 ⟷ 4 → NULL**  

**_DATA = 3_**  

Return a reference to the new list: **_1 ⟷ 2 ⟷ 3 ⟷ 4 → NULL**.  

<h4>Function Description</h4>

Complete the `sortedInsert` function.  

`sortedInsert` has two parameters:  

- `DoublyLinkedListNode` pointer `head`: a reference to the head of a doubly-linked list  
- `int data`: An integer denoting the value of the **_data_** field for the DoublyLinkedListNode you must insert into the list.  

<h4>Returns</h4>

- `DoublyLinkedListNode` pointer: a reference to the head of the list  

**Note**: Recall that an empty list (i.e., where **_head = NULL_**) and a list with one element are sorted lists.  

<h4>Input Format</h4>

The first line contains an integer **_t_**, the number of test cases.  

Each of the test cases is in the following format:  

- The first line contains an integer **_n_**, the number of elements in the linked list.  
- Each of the next **_n_** lines contains an integer, the data for each node of the linked list.  
- The last line contains an integer, **_data_**, which needs to be inserted into the sorted doubly-linked list.  

<h4>Constraints</h4>

- **_1 ≤ t ≤ 10_**  
- **_1 ≤ n ≤ 1000_**  
- **_1 ≤ DoublyLinkedListNode.data ≤ 1000_**  

<h4>Sample Input</h4>

```
1
4
1
3
4
10
5
```

<h4>Sample Output</h4>

```
1 3 4 5 10
```

<h4>Explanation</h4>

The initial doubly linked list is: **_1 ⟷ 3 ⟷ 4 ⟷ 10 → NULL**.  

The doubly linked list after insertion is: **_1 ⟷ 3 ⟷ 4 ⟷ 5 ⟷ 10 → NULL**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Inserting_A_Node.cpp -o Inserting_A_Node
```

This program can be run by calling:  

```
./Inserting_A_Node
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Inserting_A_Node
```
