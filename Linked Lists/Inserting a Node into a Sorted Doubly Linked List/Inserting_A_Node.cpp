#include <bits/stdc++.h>

using namespace std;

class DoublyLinkedListNode {
public:
  int data;
  DoublyLinkedListNode *next,
  *prev;
  
  DoublyLinkedListNode(int);
};

class DoublyLinkedList {
public:
  DoublyLinkedListNode *head,
  *tail;
  
  DoublyLinkedList();
  void insert_node(int);
};

DoublyLinkedListNode *sortedInsert(DoublyLinkedListNode *, int);
void print_doubly_linked_list(DoublyLinkedListNode *, string, ofstream &);
void free_doubly_linked_list(DoublyLinkedListNode *);

int main() {
  ofstream fout("output");
  
  int t;
  cin >> t;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int t_itr = 0; t_itr < t; t_itr ++) {
    DoublyLinkedList *llist = new DoublyLinkedList();
    
    int llist_count;
    cin >> llist_count;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    for (int i = 0; i < llist_count; i ++) {
      int llist_item;
      cin >> llist_item;
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
      
      llist->insert_node(llist_item);
    }
    
    int data;
    cin >> data;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    DoublyLinkedListNode *llist1 = sortedInsert(llist->head, data);
    
    print_doubly_linked_list(llist1, " ", fout);
    fout << endl;
    
    free_doubly_linked_list(llist1);
    delete llist;
  }
  
  fout.close();
}

DoublyLinkedListNode::DoublyLinkedListNode(int node_data) {
  this->data = node_data;
  this->next = this->prev = nullptr;
}

DoublyLinkedList::DoublyLinkedList() {
  this->head = this->tail = nullptr;
}

void DoublyLinkedList::insert_node(int node_data) {
  DoublyLinkedListNode *node = new DoublyLinkedListNode(node_data);
  
  if (!this->head)
    this->head = node;
  else {
    this->tail->next = node;
    node->prev = this->tail;
  }
  this->tail = node;
}

DoublyLinkedListNode *sortedInsert(DoublyLinkedListNode *head, int data) {
  DoublyLinkedListNode *currentNode = head,
                       *prevNode = nullptr;
  
  while (currentNode != nullptr && currentNode->data < data) {
    prevNode = currentNode;
    currentNode = currentNode->next;
  }
  
  DoublyLinkedListNode *newNode = new DoublyLinkedListNode(data);
  
  // if only node
  if (currentNode == nullptr && prevNode == nullptr) {
    return newNode;
    
  // if last node
  } else if (currentNode == nullptr) {
    prevNode->next = newNode;
    newNode->prev = prevNode;
    return head;
    
  // if first node
  } else if (prevNode == nullptr) {
    newNode->prev = nullptr;
    currentNode->prev = newNode;
    newNode->next = currentNode;
    return newNode;
  
  // if middle node
  } else {
    prevNode->next = currentNode->prev = newNode;
    newNode->next = currentNode;
    newNode->prev = prevNode;
    return head;
  }
}

void print_doubly_linked_list(DoublyLinkedListNode *node, string sep, ofstream &fout) {
  while (node) {
    fout << node->data;
    node = node->next;
    
    if (node)
      fout << sep;
  }
}

void free_doubly_linked_list(DoublyLinkedListNode *node) {
  while (node) {
    DoublyLinkedListNode * temp = node;
    node = node->next;
    
    delete temp;
  }
}
