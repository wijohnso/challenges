<h3>Find Merge Point of Two Lists</h3>

Given pointers to the head nodes of **_2_** linked lists that merge together at some point, find the node where the two lists merge. The merge point is where both lists point to the same node, i.e. they reference the same memory location. It is guaranteed that the two head nodes will be different, and neither will be NULL. If the lists share a common node, return that node's **_data_** value.  

**Note**: After the merge point, both lists will share the same node pointers.

<h4>Example</h4>

In the diagram below, the two lists converge at Node x:  

```
[List #1] a--->b--->c
                \
                 x--->y--->z--->NULL
                /
    [List #2] p--->q
```

<h4>Function Description</h4>

Complete the `findMergeNode` function.  

`findMergeNode` has the following parameters:  

- `SinglyLinkedListNode` pointer `head1`: a reference to the head of the first list  
- `SinglyLinkedListNode` pointer `head2`: a reference to the head of the second list  

<h4>Returns</h4>

- `int`: the **_data_** value of the node where the lists merge  

<h4>Input Format</h4>

Do not read any input from stdin/console.  

The first line contains an integer **_t_**, the number of test cases.  

Each of the test cases is in the following format:  
The first line contains an integer, **_index_** the node number where the merge will occur.  
The next line contains an integer, **_list1Count_** that is the number of nodes in the first list.  
Each of the following **_list1Count_** lines contains a **_data_** value for a node. The next line contains an integer, **_list2Count_** that is the number of nodes in the second list.  
Each of the following **_list2Count_** lines contains a **_data_** value for a node.  

<h4>Constraints</h4>

- The lists will merge.  
- **_head1_**, **_head2 ≠ NULL_**.  
- **_head1 ≠ head2_**.  

<h4>Sample Input 0</h4>

```
1
1
3
1
2
3
1
1
```

The diagram below is a graphical representations of the lists that input nodes **_head1_** and **_head2_** are connected to.  

```
1
 \
  2--->3--->NULL
 /
1
```

<h4>Sample Input 1</h4>

```
1
2
3
1
2
3
1
1
```

The diagram below is a graphical representations of the lists that input nodes **_head1_** and **_head2_** are connected to.  

```
1--->2
      \
       3--->Null
      /
     1
```

<h4>Sample Output</h4>

```
2
3
```

<h4>Explanation</h4>

Test Case 0: As demonstrated in the diagram above, the merge node's data field contains the integer **_2_**.  
Test Case 1: As demonstrated in the diagram above, the merge node's data field contains the integer **_3_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Find_Merge_Point.cpp -o Find_Merge_Point
```

This program can be run by calling:  

```
./Find_Merge_Point
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -v ./Find_Merge_Point
```
