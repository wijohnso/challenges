#include <bits/stdc++.h>

using namespace std;

class SinglyLinkedListNode {
  public:
    int data;
    SinglyLinkedListNode *next;
    
    SinglyLinkedListNode(int);
};

class SinglyLinkedList {
  public:
    SinglyLinkedListNode *head,
                         *tail;
    SinglyLinkedList();
    void insert_node(int);
};

int findMergeNode(SinglyLinkedListNode *, SinglyLinkedListNode *);
void print_singly_linked_list(SinglyLinkedListNode *, string, ofstream &);
void free_singly_linked_list(SinglyLinkedListNode *);

int main() {
  int tests;
  cin >> tests;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int tests_itr = 0; tests_itr < tests; tests_itr ++) {
    int index;
    cin >> index;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    SinglyLinkedList *llist1 = new SinglyLinkedList();
    
    int llist1_count;
    cin >> llist1_count;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    for (int i = 0; i < llist1_count; i ++) {
      int llist1_item;
      cin >> llist1_item;
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
      
      llist1->insert_node(llist1_item);
    }
    
    SinglyLinkedList *llist2 = new SinglyLinkedList();
    
    int llist2_count;
    cin >> llist2_count;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    for (int i = 0; i < llist2_count; i ++) {
      int llist2_item;
      cin >> llist2_item;
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
      
      llist2->insert_node(llist2_item);
    }
    
    SinglyLinkedListNode *ptr1 = llist1->head,
                         *ptr2 = llist2->head;
    
    for (int i = 0; i < index; i ++) {
      ptr1 = ptr1->next;
    }
      
    for (int i = 1; i < llist2_count; i ++) {
      ptr2 = ptr2->next;
    }
      
    SinglyLinkedListNode *holder = ptr2->next;
    ptr2->next = ptr1;
    
    cout << findMergeNode(llist1->head, llist2->head) << endl;
    
    ptr2->next = holder;
    free_singly_linked_list(llist1->head);
    delete llist1;
    free_singly_linked_list(llist2->head);
    delete llist2;
  }
  
  return 0;
}

SinglyLinkedListNode::SinglyLinkedListNode(int node_data) {
  this->data = node_data;
  this->next = nullptr;
}

SinglyLinkedList::SinglyLinkedList() {
  this->head = this->tail = nullptr;
}

void SinglyLinkedList::insert_node(int node_data) {
  SinglyLinkedListNode *node = new SinglyLinkedListNode(node_data);
  
  if (!this->head)
    this->head = node;
  else
    this->tail->next = node;
  
  this->tail = node;
}

int findMergeNode(SinglyLinkedListNode *head1, SinglyLinkedListNode *head2) {
  // local variables
  SinglyLinkedListNode *node1 = head1,  // traverses llist1
                       *node2 = head2;  // traverses llist2
  
  while (node1 != nullptr) {
    while (node2 != nullptr) {
      if (node1 == node2)
        return node1->data;
      
      node2 = node2->next;
    }
    node2 = head2;
    node1 = node1->next;
  }
  
  return 0;
}

void print_singly_linked_list(SinglyLinkedListNode *node, string sep, ofstream &fout) {
  while (node) {
    fout << node->data;
    node = node->next;
    if (node)
      fout << sep;
  }
}

void free_singly_linked_list(SinglyLinkedListNode *node) {
  while (node != nullptr) {
    SinglyLinkedListNode *temp = node;
    node = node->next;
    
    delete temp;
    temp = nullptr;
  }
}
