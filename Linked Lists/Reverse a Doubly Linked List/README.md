<h3>Reverse a Doubly Linked List</h3>

Given the pointer to the head node of a doubly linked list, reverse the order of the nodes in place. That is, change the next and prev pointers of the nodes so that the direction of the list is reversed. Return a reference to the head node of the reversed list.  

**Note**: The head node might be `NULL` to indicate that the list is empty.  

<h4>Function Description</h4>

Complete the `reverse` function.  

`reverse` has the following parameter(s):  

- `DoublyLinkedListNode head`: a reference to the head of a DoublyLinkedList  

<h4>Returns</h4>
- `DoublyLinkedListNode`: a reference to the head of the reversed list  

<h4>Input Format</h4>

The first line contains an integer **_t_**, the number of test cases.  

Each test case is of the following format:  

- The first line contains an integer **_n_**, the number of elements in the linked list.  
The next **_n_** lines contain an integer each denoting an element of the linked list.  

<h4>Constraints</h4>

- **_1 ≤ t ≤ 10_**  
- **_0 ≤ n ≤ 1000_**  
- **_0 ≤ DoublyLinkedListNode.data ≤ 1000_**  

<h4>Output Format</h4>

Return a reference to the head of your reversed list. The provided code will print the reverse array as a one line of space-separated integers for each test case.  

<h4>Sample Input</h4>

```
1
4
1
2
3
4
```

<h4>Sample Output</h4>

```
4 3 2 1 
```

<h4>Explanation</h4>

The initial doubly linked list is: **_1 ⟷ 2 ⟷ 3 ⟷ 4 → NULL_**  
The reversed doubly linked list is: **_4 ⟷ 3 ⟷ 2 ⟷ 1 → NULL_**  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Reverse_A_List.cpp -o Reverse_A_List
```

This program can be run by calling:  

```
./Reverse_A_List
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Reverse_A_List
```
