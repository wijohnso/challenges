#include <bits/stdc++.h>

using namespace std;

class DoublyLinkedListNode {
  public:
    int data;
    DoublyLinkedListNode *next,
                         *prev;
    
    DoublyLinkedListNode(int);
};

class DoublyLinkedList {
  public:
    DoublyLinkedListNode *head,
                         *tail;
    
    DoublyLinkedList();
    void insert_node(int);
};

DoublyLinkedListNode *reverse(DoublyLinkedListNode *);
void print_doubly_linked_list(DoublyLinkedListNode *, string, ofstream &);
void free_doubly_linked_list(DoublyLinkedListNode *);


int main() {
  ofstream fout("output");
  
  int t;
  cin >> t;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int t_itr = 0; t_itr < t; t_itr ++) {
    DoublyLinkedList *llist = new DoublyLinkedList();
    
    int llist_count;
    cin >> llist_count;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    for (int i = 0; i < llist_count; i ++) {
      int llist_item;
      cin >> llist_item;
      cin.ignore(numeric_limits<streamsize>::max(), '\n');
      
      llist->insert_node(llist_item);
    }
  
    DoublyLinkedListNode *llist1 = reverse(llist->head);
    
    print_doubly_linked_list(llist1, " ", fout);
    fout << endl;
    
    free_doubly_linked_list(llist1);
    delete llist;
  }
  
  fout.close();
}

DoublyLinkedListNode::DoublyLinkedListNode(int node_data) {
  this->data = node_data;
  this->next = this->prev = nullptr;
}

DoublyLinkedList::DoublyLinkedList()  {
  this->head = this->tail = nullptr;
}

void DoublyLinkedList::insert_node(int node_data) {
  DoublyLinkedListNode *node = new DoublyLinkedListNode(node_data);
  
  if (!this->head)
    this->head = node;
  else {
    this->tail->next = node;
    node->prev = this->tail;
  }
  this->tail = node;
}

DoublyLinkedListNode *reverse(DoublyLinkedListNode *head) {
  DoublyLinkedListNode *front = head,
  *middle = nullptr,
  *end = nullptr;
  
  while (front != nullptr) {
    end = middle;
    middle = front;
    front = front->next;
    
    middle->next = end;
    middle->prev = front;
  }
  
  middle->next = end;
  middle->prev = nullptr;
  return middle;
}

void print_doubly_linked_list(DoublyLinkedListNode *node, string sep, ofstream &fout) {
  while (node) {
    fout << node->data;
    node = node->next;
    
    if (node)
      fout << sep;
  }
}

void free_doubly_linked_list(DoublyLinkedListNode *node) {
  while (node) {
    DoublyLinkedListNode * temp = node;
    node = node->next;
    
    delete temp;
  }
}
