#include <bits/stdc++.h>

using namespace std;

class SinglyLinkedListNode {
  public:
    int data;
    SinglyLinkedListNode *next;
    
    SinglyLinkedListNode(int);
};

class SinglyLinkedList {
  public:
    SinglyLinkedListNode *head,
                         *tail;
                         
    SinglyLinkedList();
    void insert_node(int);
};

SinglyLinkedListNode *insertNodeAtPosition(SinglyLinkedListNode *, int, int);
void print_singly_linked_list(SinglyLinkedListNode *, string, ofstream &);
void free_singly_linked_list(SinglyLinkedListNode *);

int main() {
  ofstream fout("output");
  
  SinglyLinkedList *llist = new SinglyLinkedList();
  
  int llist_count;
  cin >> llist_count;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  for (int i = 0; i < llist_count; i ++) {
    int llist_item;
    cin >> llist_item;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    
    llist->insert_node(llist_item);
  }
  
  int data;
  cin >> data;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  int position;
  cin >> position;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  SinglyLinkedListNode *llist_head = insertNodeAtPosition(llist->head, data, position);
  
  print_singly_linked_list(llist_head, " ", fout);
  cout << endl;
  
  free_singly_linked_list(llist_head);
  fout.close();
  
  return 0;
}

SinglyLinkedListNode::SinglyLinkedListNode(int node_data) {
  this->data = node_data;
  this->next = nullptr;
}

SinglyLinkedList::SinglyLinkedList() {
  this->head = this->tail = nullptr;
}

void SinglyLinkedList::insert_node(int node_data) {
  SinglyLinkedListNode *node = new SinglyLinkedListNode(node_data);
  
  if (!this->head)
    this->head = node;
  else
    this->tail->next = node;
  
  this->tail = node;
}

SinglyLinkedListNode *insertNodeAtPosition(SinglyLinkedListNode *head, int data, int position) {
  int currentPos = 0;
  SinglyLinkedListNode *currentNode = head,
  *prevNode = nullptr;
  
  while (currentNode != nullptr && currentPos < position) {
    prevNode = currentNode;
    currentNode = currentNode->next;
    currentPos ++;
  }
  
  SinglyLinkedListNode *newNode = new SinglyLinkedListNode(data);
  
  // if only node
  if (currentNode == nullptr && prevNode == nullptr) {
    return newNode;
    
    // if last node
  } else if (currentNode == nullptr) {
    prevNode->next = newNode;
    return head;
    
    // if first node
  } else if (prevNode == nullptr) {
    newNode->next = currentNode;
    return newNode;
    
    // if middle node
  } else {
    prevNode->next = newNode;
    newNode->next = currentNode;
    return head;
  }
}

void print_singly_linked_list(SinglyLinkedListNode *node, string sep, ofstream& fout) {
  while (node) {
    fout << node->data;
    node = node->next;
    if (node)
      fout << sep;
  }
}

void free_singly_linked_list(SinglyLinkedListNode *node) {
  while (node) {
    SinglyLinkedListNode *temp = node;
    node = node->next;
    free(temp);
  }
}
