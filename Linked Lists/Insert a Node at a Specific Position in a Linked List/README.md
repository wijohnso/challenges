<h3>Insert a Node at a Specific Position in a Linked List</h3>

Given the pointer to the head node of a linked list and an integer to insert at a certain position, create a new node with the given integer as its **_data_** attribute, insert this node at the desired position and return the head node.  

A position of `0` indicates head, a position of `1` indicates one node away from the head and so on. The head pointer given may be `NULL` meaning that the initial list is empty.  

<h4>Example</h4>

**_head_** refers to the first node in the list **_1 → 2 → 3_**  
**_data = 4_**  
**_position = 2_**

Insert a node at position **_2_** with **_data = 4_**. The new list is **_1 → 2 → 4 → 3_**.  

<h4>Function Description</h4>

Complete the function `insertNodeAtPosition`. It must return a reference to the head node of your finished list.  

`insertNodeAtPosition` has the following parameters:  

- `head`: a `SinglyLinkedListNode` pointer to the head of the list  
- `data`: an integer value to insert as data in your new node  
- `position`: an integer position to insert the new node, zero based indexing  

<h4>Returns</h4>

- `SinglyLinkedListNode` pointer: a reference to the head of the revised list  

<h4>Input Format</h4>

The first line contains an integer **_n_**, the number of elements in the linked list.  
Each of the next **_n_** lines contains an integer `SinglyLinkedListNode[i].data`.  
The next line contains an integer **_data_**, the data of the node that is to be inserted.  
The last line contains an integer **_position_**.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 1000_**  
- **_1 ≤ SinglyLinkedListNode[i].data ≤ 1000_**, where **_SinglyLinkedListNode[i]_** is the **_i<sup>th</sup>_** element of the linked list.  

<h4>Sample Input</h4>

```
3
16
13
7
1
2
```

<h4>Sample Output</h4>

```
16 13 1 7
```

<h4>Explanation</h4>

The initial linked list is **_16 → 13 → 7_**. Insert **_1_** at the position **_2_** which currently has **_7_** in it. The updated linked list is **_16 → 13 → 1 → 7_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Insert_A_Node.cpp -o Insert_A_Node
```

This program can be run by calling:  

```
./Insert_A_Node
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Insert_A_Node
```
