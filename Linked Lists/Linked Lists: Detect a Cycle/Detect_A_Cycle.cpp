bool has_cycle(Node *head) {
  map<Node*, int> dictionary;
  map<Node*, int>::iterator it;
  Node *currentNode = head;
  
  while (currentNode != nullptr) {
    if ((it = dictionary.find(currentNode)) != dictionary.end()) {
      return true;
    }
    dictionary.insert(pair<Node*, int>(currentNode, 1));
    currentNode = currentNode->next;
  }
  return false;
}
