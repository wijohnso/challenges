<h3>Linked Lists: Detect a Cycle</h3>

A linked list is said to contain a cycle if any node is visited more than once while traversing the list. For example, in the following graph there is a cycle formed when node **_5_** points back to node **_3_**.  

<h4>Function Description</h4>

Complete the function `has_cycle`. It must return a boolean `true` if the graph contains a cycle, or `false` otherwise.  

`has_cycle` has the following parameter(s):  

- `head`: a pointer to a `Node` object that points to the head of a linked list.  

**Note**: If the list is empty, **_head_** will be null.  

<h4>Input Format</h4>

There is no input for this challenge. A random linked list is generated at runtime and passed to your function.  

<h4>Constraints</h4>

- **_0 ≤ list_size ≤ 100_**  

<h4>Output Format</h4>

If the list contains a cycle, your function must return true. If the list does not contain a cycle, it must return false. The binary integer corresponding to the boolean value returned by your function is printed to stdout by our hidden code checker.

<h4>Sample Input</h4>

The following linked lists are passed as arguments to your function:

```
???
```

<h4>Sample Output</h4>

```
0
1
```

<h4>Explanation</h4>

1. The first list has no cycle, so we return false and the hidden code checker prints **_0_** to stdout.  
2. The second list has a cycle, so we return true and the hidden code checker prints **_1_** to stdout.  

<h4>NOTE</h4>

This challenge was not accompanied by source code for data structures or `main()`, so it cannot be compiled or tested outside of the hackerrank ecosystem. :(  
