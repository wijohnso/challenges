<h3>Strings: Making Anagrams</h3>

A student is taking a cryptography class and has found anagrams to be very useful. Two strings are anagrams of each other if the first string's letters can be rearranged to form the second string. In other words, both strings must contain the same exact letters in the same exact frequency. For example, `bacdc` and `dcbac` are anagrams, but `bacdc` and `dcbad` are not.  

The student decides on an encryption scheme that involves two large strings. The encryption is dependent on the minimum number of character deletions required to make the two strings anagrams. Determine this number.  

Given two strings, **_a_** and **_b_**, that may or may not be of the same length, determine the minimum number of character deletions required to make **_a_** and **_b_** anagrams. Any characters can be deleted from either of the strings.  

<h4>Example</h4>

**_a = 'cde'_**  
**_b = 'cdf'_**  

Delete **_e_** from **_a_** and **_f_** from **_b_** so that the remaining strings are **_cd_** and **_dc_** which are anagrams. This takes **_2_** character deletions.  

<h4>Function Description</h4>

Complete the `makeAnagram` function.  

`makeAnagram` has the following parameter(s):  

- `string a`: a string  
- `string b`: another string  

<h4>Returns</h4>

- `int`: the minimum total characters that must be deleted  

<h4>Input Format</h4>

The first line contains a single string, **_a_**.  
The second line contains a single string, **_b_**.  

<h4>Constraints</h4>

- **_1 ≤ |a|,|b| ≤ 10<sup>4</sup>_**  
- The strings **_a_** and **_b_** consist of lowercase English alphabetic letters, ascii[a-z].  

<h4>Sample Input</h4>

```
cde
abc
```

<h4>Sample Output</h4>

```
4
```

<h4>Explanation</h4>

Delete the following characters from the strings make them anagrams:  

1. Remove `d` and `e` from `cde` to get `c`.  
2. Remove `a` and `b` from `abc` to get `c`.  

It takes **_4_** deletions to make both strings anagrams.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Making_Anagrams.cpp -o Making_Anagrams
```

This program can be run by calling:  

```
./Making_Anagrams
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Making_Anagrams
```
