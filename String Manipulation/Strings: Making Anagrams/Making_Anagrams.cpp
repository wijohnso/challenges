#include <bits/stdc++.h>

using namespace std;

int makeAnagram(string, string);

int main() {
  string a;
  getline(cin, a);
  
  string b;
  getline(cin, b);
  
  int res = makeAnagram(a, b);
  
  cout << res << endl;
  
  return 0;
}

int makeAnagram(string a, string b) {
  map<char, int> dictA,
                 dictB;
  map<char, int>::iterator it;
  int returnVal = 0;
  
  for (int i = 0; i < a.length(); i ++) {
    if ((it = dictA.find(a[i])) != dictA.end()) {
      int count = it->second;
      dictA.erase(a[i]);
      dictA.insert(pair<char, int>(a[i], ++count));
    } else {
      dictA.insert(pair<char, int>(a[i], 1));
    }
  }
  
  for (int i = 0; i < b.length(); i ++) {
    if ((it = dictA.find(b[i])) == dictA.end()) {
      // removing value from b
      returnVal ++;
    } else {
      int count = it->second;
      dictA.erase(b[i]);
      if (--count > 0)
        dictA.insert(pair<char, int>(b[i], count));
    }
  }
  
  for (it = dictA.begin(); it != dictA.end(); it ++) {
    // removing remaining values from a
    returnVal += it->second;
  }
  
  return returnVal;
}
