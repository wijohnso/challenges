#include <bits/stdc++.h>

using namespace std;

int alternatingCharacters(string);

int main() {
  int q;
  cin >> q;
  
  for (int q_itr = 0; q_itr < q; q_itr ++) {
    string s;
    getline(cin, s);
    
    int result = alternatingCharacters(s);
    
    cout << result << endl;
  }
  
  return 0;
}

int alternatingCharacters(string s) {
  char current1 = 'A',
       current2 = 'B';
  int count1 = 0,
      count2 = 0;
  
  for (int i = 0; i < s.length(); i ++) {
    if (s[i] == current1)
      count1 ++;
    else
      current1 = s[i];
    
    if (s[i] == current2)
      count2 ++;
    else
      current2 = s[i];
  }
  
  if (count1 > count2)
    return count2;
  else
    return count1;
}
