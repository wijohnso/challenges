<h3>Alternating Characters</h3>

You are given a string containing characters **_A_** and **_B_** only. Your task is to change it into a string such that there are no matching adjacent characters. To do this, you are allowed to delete zero or more characters in the string.  

Your task is to find the minimum number of required deletions.  

Example

**_s = AABAAB_**  

Remove an **_A_** at positions **_0_** and **_3_** to make **_s = ABAB_** in **_2_** deletions.  

<h4>Function Description</h4>

Complete the `alternatingCharacters` function.  

`alternatingCharacters` has the following parameter(s):  

- `string s`: a string  

<h4>Returns</h4>

- `int`: the minimum number of deletions required  

<h4>Input Format</h4>

The first line contains an integer **_q_**, the number of queries.  
The next lines each contain a string **_s_** to analyze.  

<h4>Constraints</h4>


- **_1 ≤ a ≤ 10_**  
- **_1 ≤ length of s ≤ 10<sup>5</sup>_**  
- Each string **_s_** will consist only of characters **_A_** and **_B_**.  

<h4>Sample Input</h4>

```
5
AAAA
BBBBB
ABABABAB
BABABA
AAABBB
```

<h4>Sample Output</h4>

```
3
4
0
0
4
```

<h4>Explanation</h4>

**_AAAA → A_** (3 deletions)  
**_BBBBB → B_** (4 deletions)  
**_ABABABABAB → ABABABABAB_** (0 deletions)  
**_BABABA → BABABA_** (0 deletions)  
**_AAABBB → AB_** (4 deletions)  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Alternating_Characters.cpp -o Alternating_Characters
```

This program can be run by calling:  

```
./Alternating_Characters
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Alternating_Characters
```
