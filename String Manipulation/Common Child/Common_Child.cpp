#include <bits/stdc++.h>

using namespace std;

int commonChild(string, string);

int main() {
  string s1,
         s2;
  getline(cin, s1);
  getline(cin, s2);
  
  int result = commonChild(s1, s2);
  
  cout << result << endl;
  
  return 0;
}

int commonChild(string s1, string s2) {
  int size = s1.length();
  vector<vector<int>> matrix(size + 1, vector<int>(size + 1, 0));
  
  for (int i = 0; i < size; i ++)
    for (int j = 0; j < size; j ++)
      if (s1[i] == s2[j])
        matrix[i + 1][j + 1] = 1 + matrix[i][j];
      else
        matrix[i + 1][j + 1] = max(matrix[i + 1][j], matrix[i][j + 1]);
  
  return matrix[size][size];
}
