<h3>Common Child</h3>

A string is said to be a child of a another string if it can be formed by deleting `0` or more characters from the other string. Given two strings of equal length, what's the longest string that can be constructed such that it is a child of both?  

For example, `ABCD` and `ABDC` have two children with maximum length `3`, `ABC` and `ABD`. They can be formed by eliminating either the `D` or `C` from both strings. Note that we will not consider `ABCD` as a common child because we can't rearrange characters and `ABCD ≠ ABDC`.  

<h4>Function Description</h4>

Complete the `commonChild` function. It should return the longest string which is a common child of the input strings.  

`commonChild` has the following parameter(s):  

- `s1`, `s2`: two equal length strings  

<h4>Input Format</h4>

There is one line with two space-separated strings, **_s1_** and **_s2_**.  

<h4>Constraints</h4>

- **_1 ≤ |s1|,|s2| ≤ 5000_**  
- All characters are upper case in the range `ascii[A-Z]`.  

<h4>Output Format</h4>

Print the length of the longest string **_s_**, such that is a child of both **_s1_** and **_s2_**.  

<h4>Sample Input</h4>

```
HARRY
SALLY
```

<h4>Sample Output</h4>

```
2
```

<h4>Explanation</h4>

The longest string that can be formed by deleting zero or more characters from **_HARRY_** and **_SALLY_** is **_AY_**, whose length is `2`.  

<h4>Sample Input 1</h4>

```
AA
BB
```

<h4>Sample Output 1</h4>

```
0
```

<h4>Explanation 1</h4>

**_AA_** and **_BB_** have no characters in common and hence the output is `0`.  

<h4>Sample Input 2</h4>

```
SHINCHAN
NOHARAAA
```

<h4>Sample Output 2</h4>

```
3
```

<h4>Explanation 2</h4>

The longest string that can be formed between **_SHINCHAN_** and **_NOHARAAA_** while maintaining the order is **_NHA_**.  

<h4>Sample Input 3</h4>

```
ABCDEF
FBDAMN
```

<h4>Sample Output 3</h4>

```
2
```

<h4>Explanation 3</h4>

**_BD_** is the longest child of the given strings.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Common_Child.cpp -o Common_Child
```

This program can be run by calling:  

```
./Common_Child
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Common_Child
```
