<h3>Special String Again</h3>

A string is said to be a special string if either of two conditions is met:  

- All of the characters are the same, e.g. aaa.  
- All characters except the middle one are the same, e.g. aadaa.  

A special substring is any substring of a string which meets one of those criteria. Given a string, determine how many special substrings can be formed from it.  

For example, given the string **_s = mnonopoo_**, we have the following special substrings: **_{m, n, o, n, o, p, o, o, non, ono, opo, oo}_**.  

<h4>Function Description</h4>

Complete the `substrCount` function. It should return an integer representing the number of special substrings that can be formed from the given string.  

`substrCount` has the following parameter(s):  

- `n`: an integer, the length of string `s`  
- `s`: a string  

<h4>Input Format</h4>

The first line contains an integer, **_n_**, the length of **_s_**.  
The second line contains the string **_s_**.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 10<sup>6</sup>_**  
- Each character of the string is a lowercase alphabet, **_ascii[a-z]_**.  

<h4>Output Format</h4>

Print a single line containing the count of total special substrings.  

<h4>Sample Input 0</h4>

```
5
asasd
```

<h4>Sample Output 0</h4>

```
7 
```

<h4>Explanation 0</h4>

The special palindromic substrings of **_s = asasd_** are **_{a, s, a, s, d, asa, sas}_**.  

<h4>Sample Input 1</h4>

```
7
abcbaba
```

<h4>Sample Output 1</h4>

```
10 
```

<h4>Explanation 1</h4>

The special palindromic substrings of **_s = abcbaba_** are **_{a, b, c, ba, a, b, a, bcb, bab, aba}_**.  

<h4>Sample Input 2</h4>

```
4
aaaa
```

<h4>Sample Output 2</h4>

```
10
```

<h4>Explanation 2</h4>

The special palindromic substrings of **_s = aaaa_** are **_{a, a, a, a, aa, aa, aa, aaa, aaa, aaaa}_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Special_String.cpp -o Special_String
```

This program can be run by calling:  

```
./Special_String
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Special_String
```
