#include <bits/stdc++.h>

using namespace std;

long substrCount(int, string);

int main() {
  int n;
  cin >> n;
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
  
  string s;
  getline(cin, s);
  
  long result = substrCount(s.length(), s);
  
  cout << result << endl;
  
  return 0;
}

long substrCount(int n, string s) {
  long returnVal = n;
  int  prevIndex;
  
  for (int i = 0; i < n - 1; i++) {
    prevIndex = i;
    
    for (int j = i + 1; j < n; j++) {
      // if double character
      if ((s[i] == s[j]) && (prevIndex == i))
        returnVal ++;
      
      // if special string
      else if ((s[i] == s[j]) && ((j - prevIndex) == (prevIndex - i)))
        returnVal ++;
      
      // if first pass, move prevIndex to check for longer special strings
      else if (s[i] != s[j] && prevIndex == i)
        prevIndex = j;
      
      // if nothing
      else if (s[i] != s[j])
        break;
    }
  }
  return returnVal;
}
