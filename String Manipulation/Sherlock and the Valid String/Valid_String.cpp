#include <bits/stdc++.h>

using namespace std;

string isValid(string);

int main() {
  string s;
  getline(cin, s);
  
  string result = isValid(s);
  
  cout << result << endl;
  
  return 0;
}

string isValid(string s) {
  // local variables
  map<char, int> dict;
  map<char, int>::iterator it;
  int current = 0,
      prev,
      oops = 0;
  
  for (int i = 0; i < s.length(); i ++) {
    if ((it = dict.find(s[i])) != dict.end()) {
      int count = it->second;
      dict.erase(s[i]);
      dict.insert(pair<char, int>(s[i], ++count));
    } else {
      dict.insert(pair<char, int>(s[i], 1));
    }
  }
  
  for (it = dict.begin(); it != dict.end(); it ++) {
    current = it->second;
    
    if (it == dict.begin()) {
      prev = current;
      continue;
    }
    
    if (prev != current) {
      cout << prev << " " << current << endl;
      oops ++;
      continue;
    }
  }
  
  if (oops > 1)
    return "NO";
  else
    return "YES";
}
