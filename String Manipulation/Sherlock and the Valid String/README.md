<h3>Sherlock and the Valid String</h3>

Sherlock considers a string to be valid if all characters of the string appear the same number of times. It is also valid if he can remove just **_1_** character at **_1_** index in the string, and the remaining characters will occur the same number of times. Given a string **_s_**, determine if it is valid. If so, return `YES`, otherwise return `NO`.  

<h4>Example</h4>

**_s = abc_**  

This is a valid string because frequencies are **_{a:1,b:1,c:1}_**.  

**_s = abcc_**  

This is a valid string because we can remove one **_c_** and have **_1_** of each character in the remaining string.  

**_s = abccc_**  

This string is not valid as we can only remove occurrence of **_c_**. That leaves character frequencies of **_{a:1,b:1,c:2}_**.  

<h4>Function Description</h4>

Complete the `isValid` function.  

`isValid` has the following parameter(s):  

- `string s`: a string  

<h4>Returns</h4>

- `string`: either `YES` or `NO`  

<h4>Input Format</h4>

A single string **_s_**.  

<h4>Constraints</h4>

- **_1 ≤ |s| ≤ 10<sup>5</sup>_**  
- Each character **_s[i] ∈ ascii[a - z]_**  

<h4>Sample Input 0</h4>

```
aabbcd
```

<h4>Sample Output 0</h4>

```
NO
```

<h4>Explanation 0</h4>

Given **_s = "aabbcd"_**, we would need to remove two characters, both c and d → aabb or a and b → abcd, to make it valid. We are limited to removing only one character, so **_s_** is invalid.  

<h4>Sample Input 1</h4>

```
aabbccddeefghi
```

<h4>Sample Output 1</h4>

```
NO
```

<h4>Explanation 1</h4>

Frequency counts for the letters are as follows:  

{'a': 2, 'b': 2, 'c': 2, 'd': 2, 'e': 2, 'f': 1, 'g': 1, 'h': 1, 'i': 1}  

There are two ways to make the valid string:  

- Remove **_4_** characters with a frequency of **_1_**: **_{fghi}_**.  
- Remove **_5_** characters of frequency **_2_**: **_{aabcde}_**.  

Neither of these is an option.  

<h4>Sample Input 2</h4>

```
abcdefghhgfedecba
```

<h4>Sample Output 2</h4>

```
YES
```

<h4>Explanation 2</h4>

All characters occur twice except for **_e_** which occurs **_3_** times. We can delete one instance of **_e_** to have a valid string.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Valid_String.cpp -o Valid_String
```

This program can be run by calling:  

```
./Valid_String
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Valid_String
```
