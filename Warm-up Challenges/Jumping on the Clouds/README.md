<h3>Jumping on the Clouds</h3>

There is a new mobile game that starts with consecutively numbered clouds. Some of the clouds are thunderheads and others are cumulus. The player can jump on any cumulus cloud having a number that is equal to the number of the current cloud plus **_1_** or **_2_**. The player must avoid the thunderheads. Determine the minimum number of jumps it will take to jump from the starting postion to the last cloud. It is always possible to win the game.  

For each game, you will get an array of clouds numbered **_0_** if they are safe or **_1_** if they must be avoided.  
    
<h4>Example</h4>
```
c = [0,1,0,0,0,1,0]
```

Index the array from **_0...6_**. The number on each cloud is its index in the list so the player must avoid the clouds at indices **_1_** and **_5_**. They could follow these two paths: **_0 → 2 → 4 → 6_** or **_0 → 2 → 3 → 4 → 6_**. The first path takes **_3_** jumps while the second takes **_4_**. Return **_3_**.  

<h4>Function Description</h4>

Complete the `jumpingOnClouds` function.  

`jumpingOnClouds` has the following parameter(s):  

- `int c[n]`: an array of binary integers  

<h4>Returns</h4>

- `int`: the minimum number of jumps required  

<h4>Input Format</h4>

The first line contains an integer **_n_**, the total number of clouds. The second line contains **_n_** space-separated binary integers describing clouds **_c[i]_** where **_0 ≤ i ≤ n_**.

<h4>Constraints</h4>

- **_2 ≤ n ≤ 100_**  
- **_c[i] ∈ {0,1}_**  
- **_c[0] = c[n-1] = 0_**  

<h4>Output Format</h4>

Print the minimum number of jumps needed to win the game.

<h4>Sample Input 0</h4>

```
7
0 0 1 0 0 1 0
```

<h4>Sample Output 0</h4>

```
4
```

<h4>Explanation 0:</h4>
The player must avoid **_c[2]_** and **_c[5]_**. The game can be won with a minimum of **_4_**jumps: **_0 → 1 → 3 → 4 → 6_**.

<h4>Sample Input 1</h4>

```
6
0 0 0 0 1 0
```

<h4>Sample Output 1</h4>

```
3
```

<h4>Explanation 1:</h4>

The only thundercloud to avoid is **_c[4]_**. The game can be won in **_3_** jumps: **_0 → 2 → 3 → 5_**.

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Jumping_on_the_Clouds.cpp -o Jumping_on_the_Clouds
```

This program can be run by calling:  

```
./Jumping_on_the_Clouds
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Jumping_on_the_Clouds
