#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int jumpingOnClouds(int, int *);
char *readline();
char **split_string(char *);

int main() {
  char *n_endptr,
       *n_str = readline();
  int n = strtol(n_str, &n_endptr, 10);
  
  if (n_endptr == n_str || *n_endptr != '\0')
    exit(EXIT_FAILURE);
    
  char *line = readline(),
       **c_temp = split_string(line);
  int *c = (int *)malloc(n * sizeof(int));
  
  for (int i = 0; i < n; i ++) {
    char *c_item_endptr,
         *c_item_str = *(c_temp + i);
    int c_item = strtol(c_item_str, &c_item_endptr, 10);
    
    if (c_item_endptr == c_item_str || *c_item_endptr !='\0')
      exit(EXIT_FAILURE);
    
    *(c + i) = c_item;
  }
  
  int c_count = n,
      result = jumpingOnClouds(c_count, c);
  
  fprintf(stdout, "%d\n", result);
  
  free(n_str);
  free(*c_temp);
  free(c_temp);
  free(c);
  
  return 0;
}

int jumpingOnClouds(int c_count, int *c) {
  int jumps = 0,
      currentPosition = 0;
  
  while (currentPosition < c_count - 1) {
    if (currentPosition + 2 < c_count && c[currentPosition + 2] != 1) 
      currentPosition += 2;
    else
      currentPosition += 1;
    jumps ++;
  }
  return jumps;
}

char *readline() {
  size_t alloc_length = 1024,
  data_length = 0;
  char *data = (char *)malloc(alloc_length);
  
  while(true) {
    char *cursor = data + data_length,
    *line = fgets(cursor, alloc_length - data_length, stdin);
    
    if(!line)
      break;
    
    data_length += strlen(cursor);
    
    if (data_length < alloc_length - 1 || data[data_length - 1] == '\n')
      break;
    
    size_t new_length = alloc_length << 1;
    data = (char *)realloc(data, new_length);
    
    if(!data)
      break;
    
    alloc_length = new_length;
  }
  
  if (data[data_length - 1] == '\n')
    data[data_length - 1] = '\0';
  
  data = (char *)realloc(data, data_length);
  
  return data;
}

char **split_string(char *str) {
  char **splits = NULL,
  *token = strtok(str, " ");
  int spaces = 0;
  
  while (token) {
    splits = (char **)realloc(splits, sizeof(char*) * ++spaces);
    
    if (!splits)
      return splits;
    
    splits[spaces - 1] = token;
    
    token = strtok(NULL, " ");
  }
  return splits;
}
