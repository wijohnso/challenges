<h3>Counting Valleys</h3>

An avid hiker keeps meticulous records of their hikes. During the last hike that took exactly **_steps_** steps, for every step it was noted if it was an uphill, **_U_**, or a downhill, **_D_** step. Hikes always start and end at sea level, and each step up or down represents a **_1_** unit change in altitude. We define the following terms:  

- A mountain is a sequence of consecutive steps above sea level, starting with a step up from sea level and ending with a step down to sea level.  
- A valley is a sequence of consecutive steps below sea level, starting with a step down from sea level and ending with a step up to sea level.  

Given the sequence of up and down steps during a hike, find and print the number of valleys walked through.  

<h4>Example</h4>

```
steps = 8 path = [DDUUUUDD]
```

The hiker first enters a valley **_2_** units deep. Then they climb out and up onto a mountain **_2_** units high. Finally, the hiker returns to sea level and ends the hike.  

<h4>Function Description</h4>

Complete the countingValleys function.  

countingValleys has the following parameter(s):  

- `int steps`: the number of steps on the hike  
- `string path`: a string describing the path  

<h4>Returns</h4>

- `int`: the number of valleys traversed  

<h4>Input Format</h4>

The first line contains an integer **_steps_**, the number of steps in the hike.  
The second line contains a single string **_path_**, of **_steps_** characters that describe the path.  

<h4>Constraints</h4>

- **_2 ≤ steps ≤ 10<sup>6</sup>_**  
- **_path[i] ∈ {U D}_**  

<h4>Sample Input</h4>

```
8
UDDDUDUU
```

<h4>Sample Output</h4>

```
1
```


<h4>Explanation</h4>

If we represent _ as sea level, a step up as /, and a step down as \, the hike can be drawn as:  

```
_/\      _
   \    /
    \/\/
```

The hiker enters and leaves one valley.  

<h4>Compiling and Testing</h4>
This program can be compiled with g++ in a Linux environment as such:  

```
g++ Counting_Valleys.cpp -o Counting_Valleys
```

This program can be run by calling:  

```
./Counting_Valleys
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Counting_Valleys
```
