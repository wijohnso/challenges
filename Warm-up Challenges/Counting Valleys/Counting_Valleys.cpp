#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int countingValleys(int, char *);
char *readline();

int main() {
  char *n_endptr,
       *n_str = readline();
  int n = strtol(n_str, &n_endptr, 10);
  
  if (n_endptr == n_str || *n_endptr != '\0')
    exit(EXIT_FAILURE);
  
  char *s = readline();
  int result = countingValleys(n, s);
  
  fprintf(stdout, "%d\n", result);
  
  free(n_str);
  free(s);
  
  return 0;
}

int countingValleys(int n, char *s) {
  // starting elevation at sea level
  int elevation = 0,
      returnValue = 0;
  bool below = false;
  
  for (int i = 0;  i < n; i ++) {
    if (s[i] == 'U') {
      elevation ++;
    } else {
      elevation --;
      if (elevation < 0)
        below = true;
    }
    if(!elevation && below) {
      returnValue ++;
      below = false;
    }
  }
  return returnValue;
}

char *readline() {
  size_t alloc_length = 1024,
  data_length = 0;
  char *data = (char *)malloc(alloc_length);
  
  while(true) {
    char *cursor = data + data_length,
    *line = fgets(cursor, alloc_length - data_length, stdin);
    
    if(!line)
      break;
    
    data_length += strlen(cursor);
    
    if (data_length < alloc_length - 1 || data[data_length - 1] == '\n')
      break;
    
    size_t new_length = alloc_length << 1;
    data = (char *)realloc(data, new_length);
    
    if(!data)
      break;
    
    alloc_length = new_length;
  }
  
  if (data[data_length - 1] == '\n')
    data[data_length - 1] = '\0';
  
  data = (char *)realloc(data, data_length);
  
  return data;
}
