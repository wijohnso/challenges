#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int sockMerchant(int, int, int *);
char *readline();
char **split_string(char *);

int main() {
  char *n_endptr,
       *n_str = readline();
  int  n = strtol(n_str, &n_endptr, 10);
  
  if (n_endptr == n_str || *n_endptr != '\0')
    exit(EXIT_FAILURE);
  
  char *line = readline(),
       **ar_temp = split_string(line);
  int *ar = (int *)malloc(n * sizeof(int));
  
  for (int i = 0; i < n; i ++) {
    char *ar_item_endptr,
         *ar_item_str = *(ar_temp + i);
    int ar_item = strtol(ar_item_str, &ar_item_endptr, 10);
    
    if (ar_item_endptr == ar_item_str || *ar_item_endptr != '\0')
      exit(EXIT_FAILURE);
    
    *(ar + i) = ar_item;
  }
  
  int ar_count = n,
      result = sockMerchant(n, ar_count, ar);
  
  fprintf(stdout, "%d\n", result);
  
  free(n_str);
  free(*ar_temp);
  free(ar_temp);
  free(ar);
  return 0;
}

int sockMerchant (int n, int ar_count, int *ar) {
  // initializing arrays
  int *colors = (int *)calloc(n, sizeof(int)),
      *counts = (int *)calloc(n, sizeof(int));
  int returnValue = 0;
  
  // populating arrays
  for (int i = 0; i < n; i ++) {
    for (int j = 0; j < n; j ++) {
      if (colors[j] == 0) {
        counts[j] = 1;
        colors[j] = ar[i];
        break;
      } else if (colors[j] == ar[i]) {
        if(counts[j] == 1) {
          counts[j] --;
          returnValue ++;
        } else {
          counts[j] ++;
        }
        break;
      }
    }
  }
  
  free(colors);
  free(counts);
  
  return returnValue;
}

char *readline() {
  size_t alloc_length = 1024,
         data_length = 0;
  char *data = (char *)malloc(alloc_length);
  
  while(true) {
    char *cursor = data + data_length,
         *line = fgets(cursor, alloc_length - data_length, stdin);
    
    if(!line)
      break;
    
    data_length += strlen(cursor);
    
    if (data_length < alloc_length - 1 || data[data_length - 1] == '\n')
      break;
    
    size_t new_length = alloc_length << 1;
    data = (char *)realloc(data, new_length);
    
    if(!data)
      break;
    
    alloc_length = new_length;
  }
  
  if (data[data_length - 1] == '\n')
    data[data_length - 1] = '\0';
  
  data = (char *)realloc(data, data_length);
  
  return data;
}

char **split_string(char *str) {
  char **splits = NULL,
       *token = strtok(str, " ");
  int spaces = 0;
  
  while (token) {
    splits = (char **)realloc(splits, sizeof(char*) * ++spaces);
    
    if (!splits)
      return splits;
    
    splits[spaces - 1] = token;
    
    token = strtok(NULL, " ");
  }
  return splits;
}
