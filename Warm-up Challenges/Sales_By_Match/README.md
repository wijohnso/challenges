<h3>Sales By Match</h3>

Alex works at a clothing store. There is a large pile of socks that must be paired by color for sale. Given an array of integers representing the color of each sock, determine how many pairs of socks with matching colors there are.  

For example, there are **_n = 7_** socks with colors **_ar = [1,2,1,2,1,3,2]_**. There is one pair of color and one of color . There are three odd socks left, one of each color. The number of pairs is **_2_**.  

<h4>Function Description

Complete the sockMerchant function. It must return an integer representing the number of matching pairs of socks that are available.  

sockMerchant has the following parameter(s):  
- `n`: the number of socks in the pile  
- `ar`: the colors of each sock  

<h4>Input Format</h4>

The first line contains an integer **_n_**, the number of socks represented in **_ar_**.  
The second line contains space-separated integers describing the colors **_ar[i]_** of the socks in the pile.  

<h4>Constraints</h4>

- **_1 ≤ n ≤ 100_**  
- **_1 ≤ ar[i] ≤ 100_** where **_0 ≤ i ≤ n_**  

<h4>Output Format</h4>

Return the total number of matching pairs of socks that Alex can sell.  

<h4>Sample Input</h4>

```
9
10 20 20 10 10 30 50 10 20
````

<h4>Sample Output</h4>

```
3
```

<h4>Explanation</h4>

Alex can match three pairs of socks.  

<h4>Compiling and Testing</h4>
This program can be compiled with g++ in a Linux environment as such:  

```
g++ Sales_By_Match.cpp -o Sales_By_Match
```

This program can be run by calling:  

```
./Sales_By_Match
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Sales_By_Match
```
