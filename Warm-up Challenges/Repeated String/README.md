<h3>Repeated String</h3>

There is a string, **_s_**, of lowercase English letters that is repeated infinitely many times. Given an integer, **_n_**, find and print the number of letter a's in the first **_n_** letters of the infinite string.  

<h4>Example</h4>

```
s = 'abcac'
n = 10
```

The substring we consider is **_abcacabcac_**, the first characters of the infinite string. There are **_4_** occurrences of a in the substring.  

<h4>Function Description</h4>

Complete the `repeatedString` function.  
`repeatedString` has the following parameter(s):  

- `s`: a string to repeat  
- `n`: the number of characters to consider  

<h4>Returns</h4>

- `int`: the frequency of a in the substring  

<h4>Input Format</h4>

The first line contains a single string, **_s_**.  
The second line contains an integer, **_n_**.  

<h4>Constraints</h4>

- **_1 ≤ |s| ≤ 100_**  
- **_1 ≤ n ≤ 10<sup>12</sup>_**  
- For **_25%_** of the test cases, **_n ≤ 10<sup>6</sup>_**  

<h4>Sample Input 0</h4>

```
aba
10
```

<h4>Sample Output 0</h4>

```
7
```

<h4>Explanation 0</h4>

The first **_n = 10_** letters of the infinite string are abaabaabaa. Because there are **_7_** a's, we return **_7_**  

<h4>Sample Input 1</h4>

```
a
1000000000000
```

<h4>Sample Output 1</h4>

```
1000000000000
```

<h4>Explanation 1</h4>

Because all of the first **_n = 1000000000000_** letters of the infinite string are a, we return **_1000000000000_**.  

<h4>Compiling and Testing</h4>

This program can be compiled with g++ in a Linux environment as such:  

```
g++ Repeated_String.cpp -o Repeated_String
```

This program can be run by calling:  

```
./Repeated_String
```

Enter input (e.g. sample input) into `stdin`. The output will be printed to `stdout`.  

The absence of memory leaks and errors can be verified by calling:  

```
valgrind --leak-check=full --show-leak-kinds=all -v ./Repeated_String
