#include <math.h>
#include <stdio.h>
#include <string.h>

long repeatedString(char *, long);
char *readline();

int main() {
  char *s = readline(),
       *n_endptr,
       *n_str = readline();
  long n = strtol(n_str, &n_endptr, 10);
  
  if (n_endptr == n_str || *n_endptr != '\0')
    exit(EXIT_FAILURE);
  
  long result = repeatedString(s, n);
  
  fprintf(stdout, "%ld\n", result);
  
  free(s);
  free(n_str);
}

long repeatedString(char *s, long n) {
  int base = 0;
  for (int i = 0; i < strlen(s); i ++)
    if (s[i] == 'a')
      base ++;
  
  long multiplier = floor(n / strlen(s)),
       returnValue = base * multiplier;
  int limit = n % strlen(s);
  
  for (int i = 0; i < limit; i ++)
    if (s[i] == 'a')
      returnValue ++;
  
  return returnValue;
}

char *readline() {
  size_t alloc_length = 1024,
  data_length = 0;
  char *data = (char *)malloc(alloc_length);
  
  while(true) {
    char *cursor = data + data_length,
    *line = fgets(cursor, alloc_length - data_length, stdin);
    
    if(!line)
      break;
    
    data_length += strlen(cursor);
    
    if (data_length < alloc_length - 1 || data[data_length - 1] == '\n')
      break;
    
    size_t new_length = alloc_length << 1;
    data = (char *)realloc(data, new_length);
    
    if(!data)
      break;
    
    alloc_length = new_length;
  }
  
  if (data[data_length - 1] == '\n')
    data[data_length - 1] = '\0';
  
  data = (char *)realloc(data, data_length);
  
  return data;
}
